﻿using MAPZ_Cities_Game_take_2.Areas;
using MAPZ_Cities_Game_take_2.Buildings;
using MAPZ_Cities_Game_take_2.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Cities_Game_take_2.MVC
{


        // this class will be a Singleton
    sealed class GameModel
    {
        private static GameModel instance = new GameModel();
        private uint numberOfInstances = 0;
        
        private GameModel()
        {
            map = null;
            towns = null;
            player = 0;
            rows = columns = 0;
            numberOfInstances++;
        }

        public static GameModel Instance
        {
            get
            {
                return instance;
            }
        }

        public List<Area> GetAvailableArea(uint player)
        {
            List<Area> available = new List<Area>();
            if (player < 0 && player >= towns.Count)
            {
                return available;
            }

            List<Position> playerAreas = new List<Position>();
            List<Position> moveAreas = new List<Position>();
            foreach (Area area in towns.ElementAt((int)player).GetAreas())
            {
                Position current = area.GetPosition();
                playerAreas.Add(current);
                Position left = current.left();
                Position right = current.right();

                if (moveAreas.Contains(current.down()) == false)
                    moveAreas.Add(current.down());
                if (moveAreas.Contains(left.down()) == false)
                    moveAreas.Add(left.down());
                if (moveAreas.Contains(left) == false)
                    moveAreas.Add(left);
                if (moveAreas.Contains(left.up()) == false)
                    moveAreas.Add(left.up());

                if (moveAreas.Contains(current.up()) == false)
                    moveAreas.Add(current.up());
                if (moveAreas.Contains(right.up()) == false)
                    moveAreas.Add(right.up());
                if (moveAreas.Contains(right) == false)
                    moveAreas.Add(right);
                if (moveAreas.Contains(right.down()) == false)
                    moveAreas.Add(right.down());
            }

            for (int i = 0; i < playerAreas.Count; i++)
            {
                moveAreas.Remove(playerAreas[i]);
            }

            for (int i = 0; i < moveAreas.Count; i++)
            {
                if (moveAreas[i].col >= columns || moveAreas[i].row >= rows)
                {
                    moveAreas.RemoveAt(i);
                    i--;
                    continue;
                }

                if (moveAreas[i].col < 0 || moveAreas[i].row < 0)
                {
                    moveAreas.RemoveAt(i);
                    i--;
                    continue;
                }
            }

            foreach (Position pos in moveAreas)
            {
                Area area = GetArea((uint)pos.row, (uint)pos.col);
                if (area != null)
                {
                    //if (area.IsItCaptured() == false)
                        available.Add(area);
                }
            }

            return available;
        }

        public Area GetArea(uint row, uint col)
        {
            int subscript = (int)(row * columns + col);
            if (subscript >= map.Count)
                return null;

            return map.ElementAt(subscript);
        }

        public uint NextTown()
        {
            // obtain town resources and increase population
            towns.ElementAt((int)player).OnDayEnd();

            player++;
            if (player == towns.Count)
            {
                player = 0;
            }
            return player;
        }

        public uint player { get; private set; }
        public uint rows { get; set; }
        public uint columns { get; set; }
        public List<Area> map { get; set; }
        public List<Town> towns { get; set; }
    }
}
