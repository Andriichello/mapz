﻿using MAPZ_Cities_Game_take_2;
using MAPZ_Cities_Game_take_2.Areas;
using MAPZ_Cities_Game_take_2.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Cities_Game_take_2.MVC
{
    // Abstract Component
    public abstract class ControllerComponent : IGamePlay
    {
        protected IMapManagementFacade gameManager;
        protected IGameView gameView;

        protected ControllerComponent(IGameView gameView)
        {
            this.gameView = gameView;
            this.gameManager = new MapManagementFacade();
        }

        public void ActionSelected(uint row, uint col, string action)
        {
            Area area = GameModel.Instance.GetArea(row, col);
            if (area == null)
                return;

            bool isSuccessfull = false;
            if (action == "Capture Area")
            {
                isSuccessfull = gameManager.CapureArea(row, col);
            }
            else if (action == "Remove Building")
            {
                isSuccessfull = gameManager.RemoveBuilding(row, col);
            }
            else if (action == "Remove Obstacle")
            {
                isSuccessfull = gameManager.RemoveObstacle(row, col);
            }
            else if (action == "Place Building")
            {
                isSuccessfull = gameManager.PlaceHouse(row, col);
            }
            else if (action == "Place House")
            {
                isSuccessfull = gameManager.PlaceHouse(row, col);
            }
            else if (action == "Place Wood Works")
            {
                isSuccessfull = gameManager.PlaceWoodWorks(row, col);
            }
            else if (action == "Place Rock Works")
            {
                isSuccessfull = gameManager.PlaceRockWorks(row, col);
            }

            if (isSuccessfull == true)
            {
                gameView.UpdateCell(row, col, area.ShortStringRepresentation(), (int)GameModel.Instance.player);
                NextPlayer();
            }
            CellSelected(row, col);
        }

        public void CaptureArea(uint row, uint col)
        {
            throw new NotImplementedException();
        }

        public void CellSelected(uint row, uint col)
        {
            List<string> actions, info;
            GenerateActionAndIfoLists(row, col, out actions, out info);

            //List<Position> moves = gameManager.GetPossibleMoves(GameModel.Instance.player);
            //foreach (Position pos in moves)
            //{
            //    info.Add(pos.ToString());
            //}

            gameView.ShowCellInfo(info);
            gameView.ShowActions(actions);

        }

        public void GenerateActionAndIfoLists(uint row, uint col, out List<string> actions, out List<string> info)
        {
            Area area = GameModel.Instance.GetArea(row, col);
            actions = new List<string>();
            info = new List<string>();


            if (area == null)
            {
                //info.Add("no information available");
                //actions.Add("no actions available");
            }
            else
            {
                if (area.IsItCaptured())
                {
                    GameModel model = GameModel.Instance;
                    List<Area> areas = model.towns.ElementAt((int)model.player).GetAreas();
                    bool isTowns = false;
                    foreach (Area a in areas)
                    {
                        if (a.GetPosition().row == row || a.GetPosition().col == col)
                        {
                            isTowns = true;
                            break;
                        }
                    }

                    if (isTowns == false)
                    {
                        return;
                    }
                }

                string data = "";
                if (area.IsItCaptured() == false)
                {
                    actions.Add("Capture Area");
                }
                if (area.IsItEmpty())
                {
                    actions.Add("Place House");
                }
                if (area.IsThereResource())
                {
                    data = "Resource: ";

                    if (area.GetResource().type == Resources.RESOURCE_TYPE.WOOD)
                    {
                        data += "Wood";
                        if (area.IsThereBuilding() == false)
                            actions.Add("Place Wood Works");
                    }
                    if (area.GetResource().type == Resources.RESOURCE_TYPE.ROCK)
                    {
                        data += "Rock";
                        if (area.IsThereBuilding() == false)
                            actions.Add("Place Rock Works");
                    }
                    info.Add(data + ", " + area.GetResource().amount);
                }
                if (area.IsThereBuilding())
                {
                    data = "Building: ";
                    info.Add(data + area.GetBuilding().GetType().Name);
                    actions.Add("Remove Building");
                }
                if (area.IsThereObstacle())
                {
                    data = "Obstacle: ";
                    if (area.GetObstacle().GetResource().type == Resources.RESOURCE_TYPE.WOOD)
                    {
                        data += "Wood";
                    }
                    if (area.GetObstacle().GetResource().type == Resources.RESOURCE_TYPE.ROCK)
                    {
                        data += "Rock";
                    }

                    info.Add(data + ", " + area.GetObstacle().GetResource().amount);
                    actions.Add("Remove Obstacle");
                }
            }
        }

        public void GenerateTownInfoList(uint player, out List<string> info)
        {
            info = new List<string>();
            GameModel model = GameModel.Instance;
            if (model.towns.Count <= player)
                return;

            Town town = model.towns.ElementAt((int)player);
            info.Add("Town " + Convert.ToString(player));
            info.Add("Population: " + Convert.ToString(town.GetPopulation()));
            info.Add("Resources: ");
            foreach (Resource res in town.GetResources())
            {
                info.Add("\t" + res.ToString());
            }
        }

        public void CreateNewGame(uint rows, uint cols, uint players)
        {
            gameManager.GenerateMap(rows, cols, players);
            GameModel model = GameModel.Instance;

            List<List<string>> playground = new List<List<string>>();
            for (int i = 0; i < rows; i++)
            {
                playground.Add(new List<string>());
                for (int j = 0; j < cols; j++)
                {
                    playground[i].Add(model.map[i * (int)cols + j].ShortStringRepresentation());
                    //playground[i].Add(Convert.ToString(i * cols + j));
                }
            }
            gameView.ShowMap(playground);

            // showing town information
            List<string> townInfo;
            GenerateTownInfoList(0, out townInfo);
            gameView.ShowTownInfo(townInfo);
        }

        public void PlaceBuilding(uint row, uint col)
        {
            // do something here
            return;
        }

        public void RemoveObstacle(uint row, uint col)
        {
            // do something here
            return;
        }

        public abstract void NextPlayer();
    }

    // Concrete Component
    public class DefaultGameController : ControllerComponent
    {
        public DefaultGameController(IGameView gameView) : base(gameView)
        {

        }

        public override void NextPlayer()
        {
            // this method will just give the right to move to the next player
            GameModel.Instance.NextTown();
            return;
        }
    }

    // Abstract Decorator
    public abstract class ControllerDecorator : ControllerComponent
    {
        protected ControllerComponent component;
        
        protected ControllerDecorator(IGameView gameView) : base(gameView)
        {

        }

        public void SetComponent(ControllerComponent component)
        {
            this.component = component;
        }
        
        public override void NextPlayer()
        {
            if (component != null)
            {
                component.NextPlayer();
            }

            List<string> townInfo;
            GenerateTownInfoList(GameModel.Instance.player, out townInfo);
            gameView.ShowTownInfo(townInfo);
        }
    }

    // Concrete Decorator
    public class MultiplePlayersGameConrollerDecorator : ControllerDecorator
    {
        public MultiplePlayersGameConrollerDecorator(IGameView gameView) : base(gameView)
        {

        }

        public override void NextPlayer()
        {
            base.NextPlayer();
            // here we do not need to do extra work
        }

    }

    // Concrete Decorator
    public class AIGameConrollerDecorator : ControllerDecorator
    {
        public AIGameConrollerDecorator(IGameView gameView) : base(gameView)
        {
            
        }

        public override void NextPlayer()
        {
            base.NextPlayer();
            // Here we need to check if player that just gained the right to move 
            // is the AI. If it's not then just move on. If it is an AI then generate
            // a move and give up the right to move by calling NextPlayer()
            GameModel model = GameModel.Instance;
            List<Position> moves = gameManager.GetPossibleMoves(model.player);
            if (model.player == 0)
            {
                // it is not an AI
                List<string> info = new List<string>();
                foreach (Position pos in moves)
                {
                    info.Add(pos.ToString());
                }
                gameView.ShowCellInfo(info);
                return;
            }
            else
            {
                // it is AI, so generate a move
                for (int i= 0; i < model.rows * model.columns; i++)
                {
                    Random random = new Random();
                    if (moves.Count == 0)
                    {
                        // this player lost the game
                        return;
                    }

                    int subscript = random.Next(0, moves.Count);
                    int row = moves[subscript].row;
                    int col = moves[subscript].col;
                    Area area = model.GetArea((uint)row, (uint)col);
                    List<string> actions, info;
                    GenerateActionAndIfoLists((uint)row, (uint)col, out actions, out info);

                    if (actions.Count > 0)
                    {
                        if (actions.Contains("Capture Area"))
                        {
                            ActionSelected((uint)row, (uint)col, "Capture Area");
                            return;
                        }
                        else
                        {
                            if (actions.Count == 1 && actions[0].Equals("no actions available"))
                                continue;
                            int selection = random.Next(0, actions.Count - 1);
                            ActionSelected((uint)row, (uint)col, actions.ElementAt(selection));
                            return;
                        }
                    }
                }
            }



        }

    }

}
