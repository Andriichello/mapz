﻿using MAPZ_Cities_Game_take_2.Areas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Cities_Game_take_2.MVC
{
    // Subject
    public abstract class Subject
    {
        public uint player { get; set; }
        public uint rows { get; set; }
        public uint columns { get; set; }
        public List<Area> map { get; set; }
        public List<Town> towns { get; set; }

        public abstract Area GetArea(uint row, uint col);
        public uint NextTown()
        {
            player++;
            if (player == towns.Count)
            {
                player = 0;
            }

            return player;
        }
    }

    // Proxy
    // Concrete Subject
    public class ModelSubject : Subject
    {
        public ModelSubject()
        {
            map = null;
            towns = null;
            player = 0;
            rows = columns = 0;
        }

        public override Area GetArea(uint row, uint col)
        {
            int subscript = (int)(row * columns + col);
            if (subscript >= map.Count)
                return null;

            return map.ElementAt(subscript);
        }
    }

    class ProxyGameModel : Subject
    {
        Subject subject;

        public override Area GetArea(uint row, uint col)
        {
            if (subject == null)
            {
                subject = new ModelSubject();
            }
            
            return subject.GetArea(row, col);
        }

        new public uint NextTown()
        {
            if (subject == null)
            {
                subject = new ModelSubject();
            }

            return subject.NextTown();
        }
    }
}
