﻿using MAPZ_Cities_Game_take_2.Areas;
using MAPZ_Cities_Game_take_2.MVC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAPZ_Cities_Game_take_2
{
    public interface IGameView
    {
        void UpdateCell(uint row, uint col, string text, int player);

        void ShowMap(List<List<string>> map);
        void ShowCellInfo(List<string> info);
        void ShowTownInfo(List<string> info);
        void ShowActions(List<string> actions);
    }


    public partial class PlaygroundForm : Form, IGameView
    {
        //remove the entire system menu:
        private const int WS_SYSMENU = 0x80000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~WS_SYSMENU;
                return cp;
            }
        }

        //private GameController gameController;
        private ControllerDecorator gameController;
        private Form parent;
        public PlaygroundForm(Form parent = null)
        {
            this.parent = parent;
            //this.gameController = new GameController(this);
            this.gameController = new AIGameConrollerDecorator(this);
            this.gameController.SetComponent(new DefaultGameController(this));
            InitializeComponent();
            SetUpGrid();
        }

        public PlaygroundForm(ControllerDecorator gameController)
        {
            //this.gameController = new GameController(this);
            this.gameController = gameController;
            InitializeComponent();
            SetUpGrid();
        }

        public void SetGameConroller(ControllerDecorator controller)
        {
            this.gameController = controller;
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            uint rows = 8;
            uint cols = 8;

            gameController.CreateNewGame(rows, cols, 2);
        }

        private void btnBackToMenu_Click(object sender, EventArgs e)
        {

        
        }

        #region MAP
        private void SetUpGrid()
        {
            mapGridView.AllowUserToAddRows = false;
            mapGridView.AllowUserToResizeColumns = false;
            mapGridView.AllowUserToResizeRows = false;

            mapGridView.ColumnHeadersVisible = false;
            mapGridView.RowHeadersVisible = false;
            mapGridView.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            mapGridView.GridColor = Color.Black;
        }

        public void ShowMap(List<List<string>> map)
        {
            int sideSize = mapGridView.Width / map[0].Count;

            mapGridView.ColumnCount = (int)map[0].Count;
            mapGridView.Rows.Clear();
            for (int i = 0; i < map.Count; i++)
            {
                string[] rowData = new string[map[i].Count];
                for (int j = 0; j < map[i].Count; j++)
                {
                    rowData[j] = map[i][j];
                }
                mapGridView.Rows.Add(rowData);
                mapGridView.Rows[i].Height = sideSize;
            }

            foreach (DataGridViewColumn column in mapGridView.Columns)
            {
                column.Width = sideSize;
            }
        }


        #endregion

        private void PlaygroundForm_Load(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lbActions_DoubleClick(object sender, EventArgs e)
        {
            if (mapGridView.SelectedCells.Count == 0)
                return;

                gameController.ActionSelected((uint)mapGridView.SelectedCells[0].RowIndex, (uint)mapGridView.SelectedCells[0].ColumnIndex, lbActions.SelectedItem.ToString());
        }

        private void mapGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (mapGridView.SelectedCells.Count == 0)
                return;

            gameController.CellSelected((uint)mapGridView.SelectedCells[0].RowIndex, (uint)mapGridView.SelectedCells[0].ColumnIndex);
        }

        public void ShowCellInfo(List<string> info)
        {
            lbInfo.Items.Clear();
            if (info == null)
                return;

            foreach (string sentence in info)
            {
                lbInfo.Items.Add(sentence);
            }
        }

        public void ShowTownInfo(List<string> info)
        {
            lbTown.Items.Clear();
            if (info == null)
                return;

            foreach (string sentence in info)
            {
                lbTown.Items.Add(sentence);
            }
        }

        public void ShowActions(List<string> actions)
        {
            lbActions.Items.Clear();
            if (actions == null)
                return;

            foreach (string sentence in actions)
            {
                lbActions.Items.Add(sentence);
            }
        }

        private void lbInfo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void mapGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void UpdateCell(uint row, uint col, string text, int player)
        {
            Color color = Color.FromArgb(244, 255, 183);
            if (player == 1)
                color = Color.FromArgb(22, 155, 183);
            
            if (text.Length > 0 && text[0] == 'C')
                mapGridView.Rows[(int)row].Cells[(int)col].Style.BackColor = color;
            //Color.FromArgb(244, 255, 183);
            mapGridView.Rows[(int)row].Cells[(int)col].Selected = false;
            mapGridView.Rows[(int)row].Cells[(int)col].Value = text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // back button
            if (parent == null)
                return;

            parent.Show();
            this.Close();

        }


    }
}
