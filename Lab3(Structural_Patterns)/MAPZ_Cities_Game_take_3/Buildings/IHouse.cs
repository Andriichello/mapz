﻿using MAPZ_Cities_Game_take_2.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Cities_Game_take_2.Buildings
{
    [Serializable]
    public class House : Building
    {
        private static readonly uint DEFAULT_CAPACITY = 20;
        private static readonly Resource DEFAULT_INCOME = null;
        private static readonly List<Resource> DEFAULT_PRICE =
            new List<Resource>() {
                new Resource(RESOURCE_TYPE.WOOD, 30), 
                new Resource(RESOURCE_TYPE.ROCK, 10) 
            };

        public House()
            : base(DEFAULT_PRICE, DEFAULT_INCOME, DEFAULT_CAPACITY, 1)
        {
            
        }

        public override Building Clone()
        {
            House house = (House)this.MemberwiseClone();

            house.mDefaultIncome = new Resource(this.mDefaultIncome);
            house.mDefaultPrice = new List<Resource>(this.mDefaultPrice);

            house.mCurrentIncome = new Resource(this.mCurrentIncome);
            house.mIncome = new Resource(this.mCurrentIncome);
            house.mPrice = new List<Resource>(this.mPrice);

            return (Building)house;
        }
    }

}
