﻿using MAPZ_Cities_Game_take_2.MVC;
using MAPZ_Cities_Game_take_2.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Cities_Game_take_2.Areas
{
    // Strategy Pattern
    public interface IStrategy : ICloneable
    {
        uint NewPopulation(uint population);
        uint NewEnergy(uint energy);
        uint NewResource(uint amount);
    }

    public class StarvingStrategy : IStrategy
    {
        public uint NewEnergy(uint energy)
        {
            return energy + 1;
        }

        public uint NewPopulation(uint population)
        {
            double decrement = 0;
            decrement = population * 0.05;

            if (decrement < 1.0)
                decrement = 1;

            if (population <= decrement)
            {
                return 0;
            }
            else
            {
                return population - (uint)decrement;
            }
        }

        public uint NewResource(uint amount)
        {
            return (uint)(amount * 0.5);
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public class SatisfiedStrategy : IStrategy
    {
        public uint NewEnergy(uint energy)
        {
            return energy + 2;
        }

        public uint NewPopulation(uint population)
        {
            double increment = 0;
            increment = population * 0.1;

            if (increment < 1.0)
                increment = 1;

            return population + (uint)increment;
        }

        public uint NewResource(uint amount)
        {
            return (uint)(amount * 0.5);
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    [Serializable]
    public class Town : ICloneable
    {
        public IStrategy mStrategy { get; private set; }
        public List<Resource> mResources { get; private set; }
        public List<Position> mPositions { get; private set; }
        public uint mPopulation { get; private set; }
        public uint mEnergy { get; private set; }

        private Town()
        {
            mResources = new List<Resource>();
            mResources.Add(new Resource(RESOURCE_TYPE.WOOD, 300));
            mResources.Add(new Resource(RESOURCE_TYPE.ROCK, 500));
            mResources.Add(new Resource(RESOURCE_TYPE.FOOD, 1000));
            mPositions = new List<Position>();

            mPopulation = 100;
            mEnergy = 2;
            mStrategy = new SatisfiedStrategy();
        }

        public Town(List<Resource> mResources, List<Position> mPositions, uint mPopulation)
            : this()
        {
            if (mResources != null)
            {
                foreach (Resource given in mResources)
                {
                    foreach (Resource current in this.mResources)
                    {
                        if (current != null && current.type == given.type)
                        {
                            current.IncreaseAmount(given.amount);
                            break;
                        }
                    }
                }
            }
            
            if (mPositions != null)
            {
                foreach (Position area in mPositions)
                {
                    this.mPositions.Add(area);
                }
            }
            
            this.mPopulation = mPopulation;
        }

        public List<Resource> GetResources()
        {
            return mResources;
        }

        public List<Position> GetPositions()
        {
            return mPositions;
        }

        public uint GetPopulation()
        {
            return mPopulation;
        }

        public uint GetEnergy()
        {
            return this.mEnergy;
        }

        public uint CountWorkingPopulation(GameModel model)
        {
            uint count = 0;
            foreach (Position pos in mPositions)
            {
                Area area = model.GetArea(pos);
                if (area != null && area.GetBuilding() != null)
                {
                    count += area.GetBuilding().GetWorkers();
                }
            }

            return count;
        }
        

        private void IncrementResources(GameModel model)
        {
            foreach (Position pos in mPositions)
            {
                Area area = model.GetArea(pos);
                if (area == null)
                    continue;

                if (area.IsThereBuilding())
                {
                    Resource income = area.GetBuilding().ObtainIncome();
                    if (income != null)
                    {
                        uint newAmount = mStrategy.NewResource(income.amount);
                        income.ResetAmount();
                        income.IncreaseAmount(newAmount);

                        AddToResources(income);
                    }
                }
            }
        }

        private void IncrementPopulation()
        {
            mPopulation = mStrategy.NewPopulation(mPopulation);
        }

        private void IncrementEnergy()
        {
            mEnergy = mStrategy.NewEnergy(mEnergy);
        }

        public void DecrementEnergy(uint amount)
        {
            if (amount > mEnergy)
                mEnergy = 0;
            else
                mEnergy -= amount;
        }

        public void AddToResources(Resource income)
        {
            if (income == null)
                return;

            foreach (Resource resource in mResources)
            {
                if (resource != null && resource.type == income.type)
                {
                    resource.IncreaseAmount(income.amount);
                    break;
                }
            }
        }

        public void AddToPositions(Position position)
        {
            mPositions.Add(position);
        }

        private void DecrementFood()
        {
            foreach (Resource res in mResources)
            {
                if (res.type == RESOURCE_TYPE.FOOD)
                {
                    int newAmount = (int)res.amount - (int)mPopulation;

                    if (newAmount < 0)
                        newAmount = 0;
                    res.ResetAmount();
                    res.IncreaseAmount((uint)newAmount);
                }
            }
        }

        public void OnDayEnd(GameModel model)
        {
            DecrementFood();
            IncrementResources(model);
            foreach (Resource res in mResources)
            {
                if (res.type == RESOURCE_TYPE.FOOD)
                {
                    if (res.amount > mPopulation * 1.3)
                    {
                        if (mStrategy as StarvingStrategy != null)
                            mStrategy = new SatisfiedStrategy();
                    }
                    else if (res.amount <= mPopulation)
                    {
                        if (mStrategy as SatisfiedStrategy != null)
                            mStrategy = new StarvingStrategy();
                    }
                    break;
                }
            }

            IncrementPopulation();
            IncrementEnergy();
            foreach (Position pos in mPositions)
            {
                Area area = model.GetArea(pos);

                if (area != null && area.GetBuilding() != null)
                {
                    area.GetBuilding().OnDayEnd();
                }
            }
        }

        public object Clone()
        {
            Town town = (Town)MemberwiseClone();
            town.mPositions = new List<Position>();
            foreach (Position pos in mPositions)
                town.mPositions.Add(pos.Clone());

            town.mStrategy = (IStrategy)mStrategy.Clone();

            town.mResources = new List<Resource>();
            foreach (Resource res in mResources)
                town.mResources.Add(new Resource(res));

            return town;
        }
    }



    // Template Pattern
    public abstract class AbstractTown
    {
        public List<Resource> mResources { get; protected set; }
        public List<Position> mPositions { get; protected set; }
        public uint mPopulation { get; protected set; }
        public uint mEnergy { get; protected set; }

        private AbstractTown()
        {
            mResources = new List<Resource>();
            mResources.Add(new Resource(RESOURCE_TYPE.WOOD, 300));
            mResources.Add(new Resource(RESOURCE_TYPE.ROCK, 500));
            mResources.Add(new Resource(RESOURCE_TYPE.FOOD, 1000));
            mPositions = new List<Position>();

            mPopulation = 100;
            mEnergy = 2;
        }

        public AbstractTown(List<Resource> mResources, List<Position> mPositions, uint mPopulation)
            : this()
        {
            if (mResources != null)
            {
                foreach (Resource given in mResources)
                {
                    foreach (Resource current in this.mResources)
                    {
                        if (current != null && current.type == given.type)
                        {
                            current.IncreaseAmount(given.amount);
                            break;
                        }
                    }
                }
            }

            if (mPositions != null)
            {
                foreach (Position area in mPositions)
                {
                    this.mPositions.Add(area);
                }
            }

            this.mPopulation = mPopulation;
        }

        public uint CountWorkingPopulation(GameModel model)
        {
            uint count = 0;
            foreach (Position pos in mPositions)
            {
                Area area = model.GetArea(pos);
                if (area != null && area.GetBuilding() != null)
                {
                    count += area.GetBuilding().GetWorkers();
                }
            }

            return count;
        }

        protected abstract void IncrementResources(GameModel model);

        protected abstract void IncrementPopulation();

        protected abstract void IncrementEnergy();

        public void DecrementEnergy(uint amount)
        {
            if (amount > mEnergy)
                mEnergy = 0;
            else
                mEnergy -= amount;
        }

        public void AddToResources(Resource income)
        {
            if (income == null)
                return;

            foreach (Resource resource in mResources)
            {
                if (resource != null && resource.type == income.type)
                {
                    resource.IncreaseAmount(income.amount);
                    break;
                }
            }
        }

        public void AddToPositions(Position position)
        {
            mPositions.Add(position);
        }

        private void DecrementFood()
        {
            foreach (Resource res in mResources)
            {
                if (res.type == RESOURCE_TYPE.FOOD)
                {
                    int newAmount = (int)res.amount - (int)mPopulation;

                    if (newAmount < 0)
                        newAmount = 0;
                    res.ResetAmount();
                    res.IncreaseAmount((uint)newAmount);
                }
            }
        }

        // template algorithm method
        public void OnDayEnd(GameModel model)
        {
            DecrementFood();
            IncrementResources(model);
            IncrementPopulation();
            IncrementEnergy();
            foreach (Position pos in mPositions)
            {
                Area area = model.GetArea(pos);

                if (area != null && area.GetBuilding() != null)
                {
                    area.GetBuilding().OnDayEnd();
                }
            }
        }

        public object Clone()
        {
            AbstractTown town = (AbstractTown)MemberwiseClone();
            town.mPositions = new List<Position>();
            foreach (Position pos in mPositions)
                town.mPositions.Add(pos.Clone());

            town.mResources = new List<Resource>();
            foreach (Resource res in mResources)
                town.mResources.Add(new Resource(res));

            return town;
        }
    }

    public class NormalTown : AbstractTown
    {
        public NormalTown(List<Resource> mResources, List<Position> mPositions, uint mPopulation) : base(mResources, mPositions, mPopulation)
        {

        }

        protected override void IncrementEnergy()
        {
            mEnergy += 2;
        }

        protected override void IncrementPopulation()
        {
            double increment = mPopulation * 1.05;
            if (increment < 1.0 && increment > 0.0)
            {
                increment = 1;
            }

            mPopulation += (uint)increment;
        }

        protected override void IncrementResources(GameModel model)
        {
            foreach (Position pos in mPositions)
            {
                Area area = model.GetArea(pos);
                if (area == null)
                    continue;

                if (area.IsThereBuilding())
                {
                    Resource income = area.GetBuilding().ObtainIncome();
                    if (income != null)
                    {
                        uint newAmount = (uint)(0.8 * income.amount);
                        income.ResetAmount();
                        income.IncreaseAmount(newAmount);
                        AddToResources(income);
                    }
                }
            }

    }
    }

    public class EasyTown : AbstractTown
    {
        public EasyTown(List<Resource> mResources, List<Position> mPositions, uint mPopulation) : base(mResources, mPositions, mPopulation)
        {

        }

        protected override void IncrementEnergy()
        {
            mEnergy += 3;
        }

        protected override void IncrementPopulation()
        {
            double increment = mPopulation * 1.1;
            if (increment < 1.0 && increment > 0.0)
            {
                increment = 1;
            }

            mPopulation += (uint)increment;
        }

        protected override void IncrementResources(GameModel model)
        {
            foreach (Position pos in mPositions)
            {
                Area area = model.GetArea(pos);
                if (area == null)
                    continue;

                if (area.IsThereBuilding())
                {
                    Resource income = area.GetBuilding().ObtainIncome();
                    if (income != null)
                    {
                        AddToResources(income);
                    }
                }
            }

        }
    }

    /*
     public class Town
    {
        #region State Pattern
        private ITownStates mCurrentState;

        public class StarvingTown : ITownStates
        {
            private Town town;
            public StarvingTown(Town town)
            {
                this.town = town;
            }

            public void ObtainIncome()
            {
                uint percents = 50;
                foreach (Area area in town.mPositions)
                {
                    if (area != null && area.GetBuilding() != null)
                    {
                        town.AddToResources(area.GetBuilding().ObtainIncome(percents));
                    }
                }

                Resource food = null;
                for (int i = 0; i < town.mResources.Count; i++)
                {
                    if (town.mResources[i].type == RESOURCE_TYPE.FOOD)
                    {
                        food = town.mResources[i];
                        break;
                    }
                }

                if (food != null)
                {
                    if (food.amount > town.mPopulation * 1.5)
                    {
                        town.mCurrentState = new SatisfiedTown(town);
                    }
                }
            }
         
            public void IncreaseEnergy()
            {
                town.mEnergy += 1;
            }

            public void IncreasePopulation()
            {
                int newPopulation = (int)town.mPopulation;
                // calculations here
                double decrement = 0;
                decrement = newPopulation * 0.03;

                if (decrement < 1.0)
                {
                    decrement = 1;
                }
                newPopulation -= (int)decrement;
                if (newPopulation < 0)
                    newPopulation = 0;

                town.mPopulation = (uint)newPopulation;
            }
        }

        public class SatisfiedTown : ITownStates
        {
            private Town town;
            public SatisfiedTown(Town town)
            {
                this.town = town;
            }

            public void ObtainIncome()
            {
                uint percents = 100;
                foreach (Area area in town.mPositions)
                {
                    if (area != null && area.GetBuilding() != null)
                    {
                        town.AddToResources(area.GetBuilding().ObtainIncome(percents));
                    }
                }

                Resource food = null;
                for (int i = 0; i < town.mResources.Count; i++)
                {
                    if (town.mResources[i].type == RESOURCE_TYPE.FOOD)
                    {
                        food = town.mResources[i];
                        break;
                    }
                }

                if (food != null)
                {
                    if (food.amount < town.mPopulation)
                    {
                        town.mCurrentState = new StarvingTown(town);
                    }
                }
            }

            public void IncreaseEnergy()
            {
                town.mEnergy += 2;
            }

            public void IncreasePopulation()
            {
                uint newPopulation = town.mPopulation;
                // calculations here
                double increment = 0;
                if (newPopulation < 20)
                {
                    increment = newPopulation * 0.3;
                }
                else
                {
                    increment = newPopulation * 0.15;
                }

                if (increment < 1.0)
                {
                    increment = 1;
                }
                newPopulation += (uint)increment;
                town.mPopulation = newPopulation;
            }
        }
        #endregion

        private List<Resource> mResources;
        private List<Area> mPositions;
        private uint mPopulation;
        public uint mEnergy { get; private set; }

        private Town()
        {
            mResources = new List<Resource>();
            mResources.Add(new Resource(RESOURCE_TYPE.WOOD, 300));
            mResources.Add(new Resource(RESOURCE_TYPE.ROCK, 500));
            mResources.Add(new Resource(RESOURCE_TYPE.FOOD, 1000));
            mPositions = new List<Area>();

            mPopulation = 100;
            mEnergy = 2;
        }

        public Town(List<Resource> mResources, List<Area> mPositions, uint mPopulation)
            : this()
        {
            if (mResources != null)
            {
                foreach (Resource given in mResources)
                {
                    foreach (Resource current in this.mResources)
                    {
                        if (current != null && current.type == given.type)
                        {
                            current.IncreaseAmount(given.amount);
                            break;
                        }
                    }
                }
            }
            
            if (mPositions != null)
            {
                foreach (Area area in mPositions)
                {
                    this.mPositions.Add(area);
                }
            }
            
            this.mPopulation = mPopulation;
            this.mCurrentState = new SatisfiedTown(this);
        }

        public List<Resource> GetResources()
        {
            return mResources;
        }

        public uint GetPopulation()
        {
            return mPopulation;
        }

        public uint CountWorkingPopulation()
        {
            uint count = 0;
            foreach (Area area in mPositions)
            {
                if (area != null && area.GetBuilding() != null)
                {
                    count += area.GetBuilding().GetWorkers();
                }
            }

            return count;
        }
        
        private void IncrementPopulation()
        {
            mCurrentState.IncreasePopulation();
        }
        
        public void AddToResources(Resource income)
        {
            if (income == null)
                return;

            foreach (Resource resource in mResources)
            {
                if (resource != null && resource.type == income.type)
                {
                    resource.IncreaseAmount(income.amount);
                    break;
                }
            }
        }

        private void IncrementResources()
        {
            mCurrentState.ObtainIncome();
        }

        public void AddToAreas(Area capturedArea)
        {
            mPositions.Add(capturedArea);
        }

        public List<Area> GetAreas()
        {
            return this.mPositions;
        }

        public void IncrementEnergy()
        {
            mCurrentState.IncreaseEnergy();
        }

        public void DecrementEnergy(uint amount)
        {
            if (amount > mEnergy)
                mEnergy = 0;
            else
                mEnergy -= amount;
        }

        public uint GetEnergy()
        {
            return this.mEnergy;
        }

        private void DecrementFood()
        {
            foreach (Resource res in mResources)
            {
                if (res.type == RESOURCE_TYPE.FOOD)
                {
                    int newAmount = (int)res.amount - (int)mPopulation;

                    if (newAmount < 0)
                        newAmount = 0;
                    res.ResetAmount();
                    res.IncreaseAmount((uint)newAmount);
                }
            }
        }

        public void OnDayEnd()
        {
            DecrementFood();
            IncrementResources();
            IncrementPopulation();
            IncrementEnergy();
            foreach (Area area in mPositions)
            {
                if (area != null && area.GetBuilding() != null)
                {
                    area.GetBuilding().OnDayEnd();
                }
            }
        }
    }
     */
}
