﻿using MAPZ_Cities_Game_take_2.MVC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAPZ_Cities_Game_take_2
{
    public partial class MenuForm : Form
    {
        //remove the entire system menu:
        private const int WS_SYSMENU = 0x80000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~WS_SYSMENU;
                return cp;
            }
        }

        public MenuForm()
        {
            InitializeComponent();
        }

        private void btnNewGame_Click(object sender, EventArgs e)
        {
            PlaygroundForm form = new PlaygroundForm(this);
            GameModel gameModel = new GameModel();
            AIGameConrollerDecorator gameController = new AIGameConrollerDecorator(gameModel, form);
            gameController.SetComponent(new DefaultGameController(gameModel, form));
            form.SetGameConroller(gameController);
            form.Show();
            this.Hide();
        }

        private void btnMultiple_Click(object sender, EventArgs e)
        {
            PlaygroundForm form = new PlaygroundForm(this);
            GameModel gameModel = new GameModel();
            MultiplePlayersGameConrollerDecorator gameController = new MultiplePlayersGameConrollerDecorator(gameModel, form);
            gameController.SetComponent(new DefaultGameController(gameModel, form));
            form.SetGameConroller(gameController);
            form.Show();
            this.Hide();
        }

        private void MenuForm_Load(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }


}
