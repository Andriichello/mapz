﻿using MAPZ_Cities_Game_take_2.Areas;
using MAPZ_Cities_Game_take_2.MVC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAPZ_Cities_Game_take_2
{
    public interface IGameView
    {
        void ClearCell(Position position);
        void ShowCell(Position position, string text);

        void ClearMap();
        void ShowMap(List<List<string>> map);

        void ShowTown(List<Position> areas, uint player);

        void ShowCellInfo(List<string> info);
        void ShowTownInfo(List<string> info);
        void ShowActions(List<string> actions);
    }


    public partial class PlaygroundForm : Form, IGameView
    {
        //remove the entire system menu:
        private const int WS_SYSMENU = 0x80000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~WS_SYSMENU;
                return cp;
            }
        }

        private string fileName = "lastSave.dat";
        private ControllerDecorator gameController;
        private Form parent;
        public PlaygroundForm(Form parent = null)
        {
            this.parent = parent;
            //this.gameController = new GameController(this);
            this.gameController = null;
            InitializeComponent();
            SetUpGrid();
            btnGiveTurn.Enabled = false;
        }

        public PlaygroundForm(ControllerDecorator gameController)
        {
            //this.gameController = new GameController(this);
            this.gameController = gameController;
            InitializeComponent();
            SetUpGrid();
        }

        public void SetGameConroller(ControllerDecorator controller)
        {
            this.gameController = controller;
            btnGenerate_Click(this, null);
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            btnGiveTurn.Enabled = true;

            uint rows = 8;
            uint cols = 8;

            gameController.CreateNewGame(rows, cols, 2);
        }

        private void btnBackToMenu_Click(object sender, EventArgs e)
        {

        
        }

        #region MAP
        private void SetUpGrid()
        {
            mapGridView.AllowUserToAddRows = false;
            mapGridView.AllowUserToResizeColumns = false;
            mapGridView.AllowUserToResizeRows = false;

            mapGridView.ColumnHeadersVisible = false;
            mapGridView.RowHeadersVisible = false;
            mapGridView.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            mapGridView.GridColor = Color.Black;
        }
        #endregion

        private void PlaygroundForm_Load(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lbActions_DoubleClick(object sender, EventArgs e)
        {
            if (mapGridView.SelectedCells.Count == 0)
                return;

                gameController.ActionSelected((uint)mapGridView.SelectedCells[0].RowIndex, (uint)mapGridView.SelectedCells[0].ColumnIndex, lbActions.SelectedItem.ToString());
        }

        private void mapGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (mapGridView.SelectedCells.Count == 0)
                return;

            gameController.CellSelected((uint)mapGridView.SelectedCells[0].RowIndex, (uint)mapGridView.SelectedCells[0].ColumnIndex);
        }

        

        private void lbInfo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void mapGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // back button
            if (parent == null)
                return;

            parent.Show();
            this.Close();

        }

        private void btnGiveTurn_Click(object sender, EventArgs e)
        {
            if (gameController != null)
                gameController.NextPlayer();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (gameController == null)
                return;
            GameModel.Memento memento = gameController.SaveGame();

            FileStream stream = File.Create(fileName);
            var formatter = new BinaryFormatter();
            formatter.Serialize(stream, memento);
            stream.Close();
        }

        private void btnRevert_Click(object sender, EventArgs e)
        {
            if (gameController == null)
                return;
            gameController.RevertToPreviousSave();
        }


        public void UpdateCell(uint row, uint col, string text, int player)
        {
            Color color = Color.FromArgb(244, 255, 100);
            if (player == 1)
                color = Color.FromArgb(22, 155, 200);

            if (text.Length > 0 && text[0] == 'C')
                mapGridView.Rows[(int)row].Cells[(int)col].Style.BackColor = color;
            //Color.FromArgb(244, 255, 183);
            mapGridView.Rows[(int)row].Cells[(int)col].Selected = false;
            mapGridView.Rows[(int)row].Cells[(int)col].Value = text;
        }


        public void ShowMap(List<List<string>> map)
        {
            int sideSize = mapGridView.Width / map[0].Count;

            mapGridView.ColumnCount = (int)map[0].Count;
            mapGridView.Rows.Clear();
            for (int i = 0; i < map.Count; i++)
            {
                string[] rowData = new string[map[i].Count];
                for (int j = 0; j < map[i].Count; j++)
                {
                    rowData[j] = map[i][j];
                }
                mapGridView.Rows.Add(rowData);
                mapGridView.Rows[i].Height = sideSize;
            }

            foreach (DataGridViewColumn column in mapGridView.Columns)
            {
                column.Width = sideSize;
            }
        }

        public void ClearMap()
        {
            for (int i = 0; i < mapGridView.Rows.Count; i++)
            {
                for (int j = 0; j < mapGridView.Rows[i].Cells.Count; j++)
                {
                    mapGridView.Rows[i].Cells[j].Value = "";
                    mapGridView.Rows[i].Cells[j].Style.BackColor = Color.White;
                }
            }
        }

        public void ShowCell(Position position, string text)
        {
            if (mapGridView.Rows.Count <= position.row)
                return;

            if (mapGridView.Rows[position.row].Cells.Count <= position.col)
                return;

            mapGridView.Rows[position.row].Cells[position.col].Value = text;
        }

        public void ClearCell(Position position)
        {
            mapGridView.Rows[position.row].Cells[position.col].Value = "";
            mapGridView.Rows[position.row].Cells[position.col].Style.BackColor = Color.White;
        }

        public void ShowCellInfo(List<string> info)
        {
            lbInfo.Items.Clear();
            if (info == null)
                return;

            foreach (string str in info)
                lbInfo.Items.Add(str);
        }

        public void ShowTown(List<Position> areas, uint player)
        {
            Color color = Color.FromArgb(244, 255, 100);
            if (player == 1)
                color = Color.FromArgb(22, 155, 200);

            foreach (Position pos in areas)
                mapGridView.Rows[pos.row].Cells[pos.col].Style.BackColor = color;
        }
        
        public void ShowTownInfo(List<string> info)
        {
            lbTown.Items.Clear();
            if (info == null)
                return;

            foreach (string sentence in info)
            {
                lbTown.Items.Add(sentence);
            }
        }

        public void ShowActions(List<string> actions)
        {
            lbActions.Items.Clear();
            if (actions == null)
                return;

            foreach (string sentence in actions)
            {
                lbActions.Items.Add(sentence);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            var formatter = new BinaryFormatter();

            // Restore from file
            FileStream stream = File.OpenRead(fileName);
            GameModel.Memento memento = (GameModel.Memento)formatter.Deserialize(stream);
            stream.Close();
            gameController.LoadGame(memento);
        }

        private void PlaygroundForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (gameController != null)
                    gameController.RevertToPreviousSave();
            }

            if (e.KeyCode == Keys.F2)
            {
                if (gameController != null)
                    gameController.SaveGame();
            }

            if (e.KeyCode == Keys.F3)
            {
                if (gameController != null)
                {
                    var formatter = new BinaryFormatter();

                    // Restore from file
                    FileStream stream = File.OpenRead(fileName);
                    GameModel.Memento memento = (GameModel.Memento)formatter.Deserialize(stream);
                    stream.Close();
                    gameController.LoadGame(memento);
                }
            }

            if (e.KeyCode == Keys.F4)
            {
                if (gameController != null)
                {
                    GameModel.Memento memento = gameController.SaveGame();

                    FileStream stream = File.Create(fileName);
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(stream, memento);
                    stream.Close();
                }
            }


        }

        private void PlaygroundForm_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
    }
}
