﻿using MAPZ_Cities_Game_take_2.Buildings;
using MAPZ_Cities_Game_take_2.Resources;
using MAPZ_Cities_Game_take_2.Areas;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace MAPZ_Cities_Game_take_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void tbTest_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            WoodWorksFactory woodWorksFactory = new WoodWorksFactory();
            RockWorksFactory rockWorksFactory = new RockWorksFactory();

            List<Area> map = new List<Area>();
            Area area = new Area(new Resource(RESOURCE_TYPE.WOOD, 1000));
            area.PlaceBuilding(woodWorksFactory.CreateBuilding());
            map.Add(area);

            //area = new Area(new Resource(RESOURCE_TYPE.ROCK, 1000));
            //area.PlaceBuilding(rockWorksFactory.CreateBuilding());
            //map.Add(area);

            //area = new Area(new Obstacle(new Resource(RESOURCE_TYPE.WOOD, 200)));
            //map.Add(area);


            Area cloned = (Area)area.Clone();
            cloned.PlaceBuilding(rockWorksFactory.CreateBuilding());

            tbTest.AppendText("\narea\n");
            if (area.GetBuilding() != null)
            {
                tbTest.AppendText("There is a building\n");
                Resource income = area.GetBuilding().ObtainIncome();
                if (income != null)
                    tbTest.AppendText("Income:" + income.amount + "\n");
            }

            tbTest.AppendText("\ncloned area\n");
            if (cloned.GetBuilding() != null)
            {
                tbTest.AppendText("There is a building");
                Resource income = cloned.GetBuilding().ObtainIncome();
                if (income != null)
                    tbTest.AppendText("Income:" + income.amount + "\n");
            }

        }
    }
}
