﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Cities_Game_take_2.Resources
{
    public enum RESOURCE_TYPE { WOOD, ROCK, FOOD };

    [Serializable]
    public class Resource
    {
        private RESOURCE_TYPE mType;
        public RESOURCE_TYPE type
        {
            get { return mType; }
        }

        private uint mAmount;
        public uint amount
        { 
            get { return mAmount; }
        }

        public Resource(RESOURCE_TYPE mType, uint mAmount)
        {
            this.mType = mType;
            this.mAmount = mAmount;
        }

        public Resource(Resource toCopy)
        {
            if (toCopy == null)
                return;
            this.mType = toCopy.mType;
            this.mAmount = toCopy.mAmount;
        }

        public void IncreaseAmount(uint number)
        {
            this.mAmount += number;
        }

        public void ResetAmount()
        {
            this.mAmount = 0;
        }

        public override string ToString()
        {
            string str = "";
            if (type == RESOURCE_TYPE.WOOD)
                str += "Wood, ";
            else if (type == RESOURCE_TYPE.ROCK)
                str += "Rock, ";
            else if (type == RESOURCE_TYPE.FOOD)
                str += "Food, ";

            str += amount;
            return str;
        }

    }
}
