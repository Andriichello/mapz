﻿using MAPZ_Cities_Game_take_2.Areas;
using MAPZ_Cities_Game_take_2.Buildings;
using MAPZ_Cities_Game_take_2.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Cities_Game_take_2.MVC
{
    // Lab 8 
    // Patterns: State, Mememto + 1 extra pattern (exept Strategy)

    // this class will be a Singleton
    public class GameModel
    {
        public GameModel()
        {
            map = null;
            towns = null;
            player = 0;
            rows = columns = 0;
        }

        public List<Area> GetAvailableArea(uint player)
        {
            List<Area> available = new List<Area>();
            if (player < 0 && player >= towns.Count)
            {
                return available;
            }

            List<Position> playerAreas = new List<Position>();
            List<Position> moveAreas = new List<Position>();
            foreach (Position pos in towns.ElementAt((int)player).GetPositions())
            {
                Position current = pos;
                playerAreas.Add(current);
                Position left = current.left();
                Position right = current.right();

                if (moveAreas.Contains(current.down()) == false)
                    moveAreas.Add(current.down());
                if (moveAreas.Contains(left.down()) == false)
                    moveAreas.Add(left.down());
                if (moveAreas.Contains(left) == false)
                    moveAreas.Add(left);
                if (moveAreas.Contains(left.up()) == false)
                    moveAreas.Add(left.up());

                if (moveAreas.Contains(current.up()) == false)
                    moveAreas.Add(current.up());
                if (moveAreas.Contains(right.up()) == false)
                    moveAreas.Add(right.up());
                if (moveAreas.Contains(right) == false)
                    moveAreas.Add(right);
                if (moveAreas.Contains(right.down()) == false)
                    moveAreas.Add(right.down());
            }

            for (int i = 0; i < playerAreas.Count; i++)
            {
                moveAreas.Remove(playerAreas[i]);
            }

            for (int i = 0; i < moveAreas.Count; i++)
            {
                if (moveAreas[i].col >= columns || moveAreas[i].row >= rows)
                {
                    moveAreas.RemoveAt(i);
                    i--;
                    continue;
                }

                if (moveAreas[i].col < 0 || moveAreas[i].row < 0)
                {
                    moveAreas.RemoveAt(i);
                    i--;
                    continue;
                }
            }

            foreach (Position pos in moveAreas)
            {
                Area area = GetArea((uint)pos.row, (uint)pos.col);
                if (area != null)
                {
                    //if (area.IsItCaptured() == false)
                        available.Add(area);
                }
            }
            return available;
        }

        public Area GetArea(uint row, uint col)
        {
            int subscript = (int)(row * columns + col);
            if (subscript >= map.Count)
                return null;

            return map.ElementAt(subscript);
        }

        public Area GetArea(Position pos)
        {
            int subscript = (int)(pos.row * columns + pos.col);
            if (subscript >= map.Count)
                return null;

            return map.ElementAt(subscript);
        }

        public uint NextTown()
        {
            // obtain town resources and increase population
            towns.ElementAt((int)player).OnDayEnd(this);

            player++;
            if (player == towns.Count)
            {
                player = 0;
            }
            return player;
        }

        public uint player { get; private set; }
        public uint rows { get; set; }
        public uint columns { get; set; }
        public List<Area> map { get; set; }
        public List<Town> towns { get; set; }

        // Memento Pattern
        [Serializable]
        public class Memento
        {
            public uint rows { get; private set; }
            public uint columns { get; private set; }
            public uint player { get; private set; }
            public List<Area> map { get; private set; }
            public List<Town> towns { get; private set; }

            public Memento(GameModel model)
            {
                rows = model.rows;
                columns = model.columns;
                player = model.player;

                map = new List<Area>();
                foreach (Area area in model.map)
                {
                    map.Add((Area)area.Clone());
                }

                towns = new List<Town>();
                foreach (Town town in model.towns)
                {
                    towns.Add((Town)town.Clone());
                }
            }

        }

        public Memento GetMemento()
        {
            return new Memento(this);
        }

        public void RevertToMemento(GameModel.Memento memento)
        {
            rows = memento.rows;
            columns = memento.columns;
            player = memento.player;

            map = memento.map;
            towns = memento.towns;
        }
    }
}
