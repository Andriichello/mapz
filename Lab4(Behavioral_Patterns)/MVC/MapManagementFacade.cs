﻿using MAPZ_Cities_Game_take_2.Areas;
using MAPZ_Cities_Game_take_2.Buildings;
using MAPZ_Cities_Game_take_2.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Cities_Game_take_2.MVC
{
    public interface IMapManagementFacade
    {
        void GenerateMap(uint rows, uint columns, uint towns);
        bool RemoveBuilding(uint row, uint column);
        bool RemoveObstacle(uint row, uint column);
        bool PlaceWoodWorks(uint row, uint column);
        bool PlaceRockWorks(uint row, uint column);
        bool PlaceFoodWorks(uint row, uint column);
        bool PlaceHouse(uint row, uint column);
        bool CapureArea(uint row, uint column);

        List<Position> GetPossibleMoves(uint player);
    }

    public class MapManagementFacade : IMapManagementFacade
    {
        private static Random random = new Random();
        private static HouseFactory houseFactory = new HouseFactory();
        private static WoodWorksFactory woodWorksFactory = new WoodWorksFactory();
        private static RockWorksFactory rockWorksFactory = new RockWorksFactory();
        private static FoodWorksFactory foodWorksFactory = new FoodWorksFactory();
        private GameModel gameModel;

        public MapManagementFacade(GameModel gameModel)
        {
            this.gameModel = gameModel;
        }

        public void GenerateMap(uint rows, uint columns, uint towns)
        {
            gameModel.rows = rows;
            gameModel.columns = columns;

            gameModel.map = new List<Area>();

            for (int i = 0; i < rows * columns; i++)
            {
                // areas creation
                Resource resource = null;
                Obstacle obstacle = null;
                Building building = null;

                // optional creation of resource
                if (GenerateRandomBool() == true)
                {
                    resource = GenerateRandomResource(1000, 10000);

                    // optional creation of works building
                    if (GenerateRandomBool() == true)
                    {
                        if (resource.type == RESOURCE_TYPE.WOOD)
                        {
                            building = woodWorksFactory.CreateBuilding();
                        }
                        if (resource.type == RESOURCE_TYPE.ROCK)
                        {
                            building = rockWorksFactory.CreateBuilding();
                        }
                        if (resource.type == RESOURCE_TYPE.ROCK)
                        {
                            building = foodWorksFactory.CreateBuilding();
                        }
                    }
                }
                // optional creation of house
                else if (GenerateRandomBool() == true)
                {
                    building = houseFactory.CreateBuilding();
                }
                // optional creation of obstacle
                else if (GenerateRandomBool() == true)
                {
                    obstacle = GenerateRandomObstacle(20, 100);
                }

                Area area = new Area(resource, building, obstacle);
                int row, col;
                row = (int)(i / rows);
                col = (int)(i % rows);
                area.SetPosition(new Position(row, col));
                gameModel.map.Add(area);
            }


            gameModel.towns = new List<Town>();
            // creating and populating towns
            for (int i = 0; i < towns; i++)
            {
                List<Resource> townResources = new List<Resource>();
                townResources.Add(new Resource(RESOURCE_TYPE.WOOD, 300));
                townResources.Add(new Resource(RESOURCE_TYPE.ROCK, 150));
                townResources.Add(new Resource(RESOURCE_TYPE.FOOD, 1000));

                Town town = new Town(townResources, null, 100);
                if (i == 0)
                {
                    town.AddToPositions(gameModel.map.ElementAt(0).GetPosition());
                    gameModel.map.ElementAt(0).Capture();
                }
                else if (i == 1)
                {
                    town.AddToPositions(gameModel.map.Last().GetPosition());
                    gameModel.map.Last().Capture();
                }

                gameModel.towns.Add(town);
            }

        }

        private bool GenerateRandomBool(uint range = 100)
        {
            return ((random.Next(0, (int)range) % 2) == 0);
        }

        private Resource GenerateRandomResource(uint minAmount, uint maxAmount)
        {
            uint amount = (uint)random.Next((int)minAmount, (int)maxAmount);
            RESOURCE_TYPE type = GenerateRandomResourceType();

            return new Resource(type, amount);
        }

        private RESOURCE_TYPE GenerateRandomResourceType()
        {
            RESOURCE_TYPE type = RESOURCE_TYPE.WOOD;
            if (GenerateRandomBool() == true)
                type = RESOURCE_TYPE.FOOD;
            else
            {
                if (GenerateRandomBool() == true)
                    type = RESOURCE_TYPE.WOOD;
                else
                    type = RESOURCE_TYPE.ROCK;
            }

            return type;
        }

        private Obstacle GenerateRandomObstacle(uint minAmount, uint maxAmount)
        {
            uint amount = (uint)random.Next((int)minAmount, (int)maxAmount);
            RESOURCE_TYPE type = GenerateRandomResourceType();

            return new Obstacle(new Resource(type, amount));
        }

        public bool PlaceWoodWorks(uint row, uint column)
        {
            Town town = gameModel.towns.ElementAt((int)gameModel.player);
            uint actionPrice = 1;
            if (town.mEnergy < actionPrice)
                return false;
            Area area = gameModel.GetArea(row, column);
            if (area == null)
                return false;
            area.PlaceBuilding(woodWorksFactory.CreateBuilding());
            town.DecrementEnergy(actionPrice);
            return true;
        }

        public bool PlaceRockWorks(uint row, uint column)
        {
            Town town = gameModel.towns.ElementAt((int)gameModel.player);
            uint actionPrice = 1;
            if (town.mEnergy < actionPrice)
                return false;
            Area area = gameModel.GetArea(row, column);
            if (area == null)
                return false;
            area.PlaceBuilding(rockWorksFactory.CreateBuilding());
            town.DecrementEnergy(actionPrice);
            return true;
        }

        public bool PlaceFoodWorks(uint row, uint column)
        {
            Town town = gameModel.towns.ElementAt((int)gameModel.player);
            uint actionPrice = 1;
            if (town.mEnergy < actionPrice)
                return false;

            Area area = gameModel.GetArea(row, column);
            if (area == null)
                return false;
            area.PlaceBuilding(foodWorksFactory.CreateBuilding());
            town.DecrementEnergy(actionPrice);
            return true;
        }

        public bool PlaceHouse(uint row, uint column)
        {
            Town town = gameModel.towns.ElementAt((int)gameModel.player);
            uint actionPrice = 1;
            if (town.mEnergy < actionPrice)
                return false;

            Area area = gameModel.GetArea(row, column);
            if (area == null)
                return false;
            area.PlaceBuilding(houseFactory.CreateBuilding());
            town.DecrementEnergy(actionPrice);
            return true;
        }

        public bool CapureArea(uint row, uint col)
        {
            Town town = gameModel.towns.ElementAt((int)gameModel.player);
            uint actionPrice = 2;
            if (town.mEnergy < actionPrice)
                return false;

            Area area = gameModel.GetArea(row, col);
            if (area == null)
                return false;

            if (gameModel.GetAvailableArea(gameModel.player).Contains(area))
            {

                if (area.Capture())
                {
                    gameModel.towns[(int)gameModel.player].AddToPositions(area.GetPosition());
                    town.DecrementEnergy(actionPrice);
                    return true;
                }
            }
            return false;
        }

        public bool RemoveBuilding(uint row, uint col)
        {
            Town town = gameModel.towns.ElementAt((int)gameModel.player);
            uint actionPrice = 1;
            if (town.mEnergy < actionPrice)
                return false;

            Area area = gameModel.GetArea(row, col);
            if (area == null)
                return false;
            area.RemoveBuilding();
            town.DecrementEnergy(actionPrice);
            return true;
        }

        public bool RemoveObstacle(uint row, uint col)
        {
            Town town = gameModel.towns.ElementAt((int)gameModel.player);
            uint actionPrice = 1;
            if (town.mEnergy < actionPrice)
                return false;
            Area area = gameModel.GetArea(row, col);
            if (area == null)
                return false;

            Resource extra = area.RemoveObstacle().GetResource();
            if (extra != null)
                town.AddToResources(extra);
            town.DecrementEnergy(actionPrice);
            return true;
        }

        public List<Position> GetPossibleMoves(uint player)
        {
            List<Area> moves = gameModel.GetAvailableArea(player);
            List<Position> positions = new List<Position>();
            foreach (Area area in moves)
            {
                positions.Add(area.GetPosition());
            }
            return positions;
        }   
    }
}
