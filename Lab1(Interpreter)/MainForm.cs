﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INTERPRETER;

namespace INTERPRETER_WITH_UI
{
    public partial class MainForm : Form
    {
        public TextBox outputBox
        {
            get;
            private set;
        }

        private Context context;
        private Parser parser;

        public MainForm()
        {
            InitializeComponent();

            outputBox = tbOutput;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnInterpret_Click(object sender, EventArgs e)
        {
            context = new Context();
            parser = new Parser();

            string code = tbCodeEditor.Text;
            code = "{" + code + "}";


            // TODO: somthing wrong with function calls in other functions
            dynamic root = parser.Make(code, context);
            if (root == null)
                return;

            rawTreeView.Nodes.Clear();
            rawTreeView.Nodes.Add(root.ToTreeNode());

            tbOutput.Text = "";
            root.Optimize();
            optimizedTreeView.Nodes.Clear();
            optimizedTreeView.Nodes.Add(root.ToTreeNode());
            root.Interpret(context);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //x = 2;
            //x + 3 - 19;
            //x = x + 4;

            //while (x) { 2 + 4; };
            //if (x) { x - 10; };

            //function foo(a; b) {
            //    while (a) { 2 - 3; }
            //    print(a - b);
            //    a - b;
            //    result = a + b;
            //};

            //foo(10; 2);
        }

        private void tbCodeEditor_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbOutput_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnExpand_Click(object sender, EventArgs e)
        {
            if (btnExpand.Text == "expand")
            {
                rawTreeView.ExpandAll();
                btnExpand.Text = "hide";
            }
            else if (btnExpand.Text == "hide")
            {
                rawTreeView.CollapseAll();
                btnExpand.Text = "expand";
            }
        }

        private void tabOptimizedTree_Click(object sender, EventArgs e)
        {

        }

        private void btnExpandOptimizedTree_Click(object sender, EventArgs e)
        {
            if (btnExpandOptimizedTree.Text == "expand")
            {
                optimizedTreeView.ExpandAll();
                btnExpandOptimizedTree.Text = "hide";
            }
            else if (btnExpandOptimizedTree.Text == "hide")
            {
                optimizedTreeView.CollapseAll();
                btnExpandOptimizedTree.Text = "expand";
            }
        }
    }
}
