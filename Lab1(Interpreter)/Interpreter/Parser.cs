﻿using INTERPRETER.OperatorExpressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER
{
    // Нотація Бекуса Наура. Описувати нею...
    public class Parser
    {
        public Parser(string code = "")
        {

        }

        public IfExpression ParceIf(List<Token> tokens, out int length, Context context)
        {
            length = 0;

            int i = 0;
            if (tokens[i].Type != "keyword" || tokens[i].Data != "if")
            {
                return null;
            }

            // need to wrap parenthesis expression by if function expression
            if (tokens[i + 1].Type == "(")
            {
                i = i + 1;
                BaseExpression parenthesis;
                parenthesis = ParenthesisParsing(tokens.GetRange(i, tokens.Count() - i), out int len, context);
                // plus two to step over the closing parenthesis
                // so in the next token will be next one 
                // after closing parenthesis
                i += len + 2;

                BaseExpression body;
                if (tokens[i].Type == "{")
                {
                    body = BracketsParsing(tokens.GetRange(i, tokens.Count() - i), out len, context);
                    i += len + 1;
                    if (parenthesis != null && body != null)
                    {
                        length = i;
                        IfExpression ifExp = new IfExpression();
                        ifExp.Condition = parenthesis;
                        ifExp.Block = body;
                        return ifExp;
                    }
                    else
                        length = 0;
                }
            }
            return null;
        }

        public WhileExpression ParceWhile(List<Token> tokens, out int length, Context context)
        {
            length = 0;

            int i = 0;
            if (tokens[i].Type != "keyword" || tokens[i].Data != "while")
            {
                return null;
            }

            // need to wrap parenthesis expression by while function expression
            if (tokens[i + 1].Type == "(")
            {
                i = i + 1;
                BaseExpression parenthesis;
                parenthesis = ParenthesisParsing(tokens.GetRange(i, tokens.Count() - i), out int len, context);
                // plus two to step over the closing parenthesis
                // so in the next token will be next one 
                // after closing parenthesis
                i += len + 2;

                BaseExpression body;
                if (tokens[i].Type == "{")
                {
                    body = BracketsParsing(tokens.GetRange(i, tokens.Count() - i), out len, context);
                    i += len + 1;
                    if (parenthesis != null && body != null)
                    {
                        length = i;

                        WhileExpression whileExp = new WhileExpression();
                        whileExp.Condition = parenthesis;
                        whileExp.Block = body;
                        return whileExp;
                    }
                    else
                        length = 0;
                }

            }

            return null;
        }

        public FunctionDefinitionExpression ParseFunctionDefinition(List<Token> tokens, out int length, Context context)
        {
            length = 0;

            int i = 0;
            if (tokens[i].Data == "function")
            {
                // i + 1: is the "symbol" which is the name of the function
                while (i < tokens.Count && tokens[i].Type != "(")
                {
                    i = i + 1;
                }

                if (tokens[i].Type == "(")
                {
                    Token opening = new Token("(", "", 0, 0);
                    Token closing = new Token(")", "", 0, 0);

                    List<Token> block = Lexer.BlockSequence(tokens, i, opening, closing);

                    FunctionDefinitionExpression function;
                    function = new FunctionDefinitionExpression();

                    int count = 0;
                    for (int j = 0; j < block.Count(); j++)
                    {
                        if (block[j].Type == "symbol")
                        {
                            Token sym = block[j];

                            if (j + 1 == block.Count)
                            {
                                // this is the last argument without a semicolumn in the end
                                function.AppendArgument(sym.Data, null);
                                break;
                            }
                            if (block[j + 1].Type == "=")
                            {
                                j += 2;
                                int to_semicolumn = 0;
                                while (block[j + to_semicolumn].Type != ";")
                                {
                                    if (j + to_semicolumn == block.Count)
                                    {
                                        to_semicolumn--;
                                        break;
                                    }
                                    to_semicolumn++;
                                }

                                List<Token> phrase = new List<Token>();
                                for (int m = j; m < block.Count(); m++)
                                {
                                    if (block[m].Type == ";" || m == j + to_semicolumn)
                                    {
                                        break;
                                        phrase.Add(block[m]);
                                    }
                                    phrase.Add(block[m]);
                                }
                                phrase.Insert(0, opening);
                                phrase.Add(closing);
                                //Lexer.Display(phrase);

                                BaseExpression right = SingleStatementBlockParsing(phrase, out int l, opening, closing, context);
                                j += l;
                                if (right != null)
                                {
                                    object a = right.Interpret(context);
                                    function.AppendArgument(sym.Data, a);
                                }
                                else
                                    function.AppendArgument(sym.Data, null);
                            }
                            else if (block[j + 1].Type == ";")
                            {
                                function.AppendArgument(sym.Data, null);
                            }
                        }
                        else if (block[j].Data == "function")
                        {
                            if (block[j + 1].Type == "symbol")
                            {
                                Token sym = block[j + 1];
                                j += 1;
                                if (block[j + 1].Type == ";")
                                {
                                    object a = new FunctionDefinitionExpression();
                                    function.AppendArgument(block[j].Data, a);
                                }
                                else if (block[j + 1].Type == "=")
                                {
                                    j += 1;
                                    int to_semicolumn = 0;
                                    while (block[j + to_semicolumn].Type != ";")
                                    {
                                        if (block[j + to_semicolumn].Type == ")")
                                        {
                                            if (block[j + to_semicolumn + 1].Type == "{")
                                            {
                                                //to_semicolumn++;
                                                break;
                                            }
                                        }
                                        to_semicolumn++;
                                    }

                                    List<Token> phrase = block.GetRange(j + 1, block.Count() - to_semicolumn);
                                    phrase.Insert(0, opening);
                                    phrase.Add(closing);

                                    BaseExpression right = SingleStatementBlockParsing(phrase, out int l, opening, closing, context);
                                    j += l;

                                    if (right != null)
                                    {
                                        object a = right.Interpret(context);
                                        function.AppendArgument(sym.Data, a);
                                    }
                                    else
                                        function.AppendArgument(sym.Data, new FunctionDefinitionExpression());
                                }
                            }
                        }

                        count++;
                        if (block[j].Type == ";")
                        {
                            count = 0;
                        }

                    }
                    /*
                    foreach (var t in block)
                    {
                        if (t.Type == "symbol")
                        {
                            function.AppendArgument(t.Data, null);
                        } 
                    }   
                    */

                    if (block.Count() > 0)
                        i = i + block.Count();

                    while (tokens[i].Type != "{")
                    {
                        i = i + 1;
                    }

                    length = i;

                    opening = new Token("{", "", 0, 0);
                    closing = new Token("}", "", 0, 0);

                    BracketsExpression body;
                    body = MultipleStatementBlockParsing(tokens.GetRange(i, tokens.Count() - i), out int len, opening, closing, function.LocalContext);

                    length += len;

                    function.Block = body;
                    return function;
                }
            }

            return null;
        }

        public FunctionExpression ParseFunctionCall(List<Token> tokens, out int length, Context context)
        {
            length = 0;

            int i = 0;
            if (tokens[i].Type == "symbol")
            {
                i = i + 1;
                if (tokens[i].Type == "(")
                {
                    Token opening = new Token("(", "", 0, 0);
                    Token closing = new Token(")", "", 0, 0);


                    FunctionExpression function;
                    function = new FunctionExpression();

                    // now we have to find a function definition
                    object definition = null;
                    definition = context.GetValue(tokens[i - 1].Data);
                    if (definition is FunctionDefinitionExpression)
                    {
                        function.Function = (FunctionDefinitionExpression)definition;
                    }
                    //else if (definition is RegexpFunction)
                    else
                    {
                        function = (FunctionExpression)definition;
                    }

                    //function.Function = (FunctionDefinitionExpression)

                    List<Token> argumentsBlock = Lexer.BlockSequence(tokens, i, new Token("(", "(", -1, -1), new Token(")", ")", -1, -1));
                    //List<Token> argumentsBlock = tokens.GetRange(i, tokens.Count() - i);
                    //if (argumentsBlock.Last().Type != ";")
                    //{
                    //    for (int k = argumentsBlock.Count - 1; k >= 0; k--)
                    //    {
                    //        if (argumentsBlock.ElementAt(k).Type == ")")
                    //        {
                    //            if (argumentsBlock.ElementAt(k).Type != ";")
                    //                argumentsBlock.Insert(k, new Token(";", ";", -1, -1));
                    //            break;
                    //        }
                    //    }
                    //}
                    if (argumentsBlock.Count > 0 && argumentsBlock.Last().Data != ";")
                    {
                        argumentsBlock.Add(new Token(";", ";", -1, -1));
                        i = i - 1;
                    }
                    argumentsBlock.Insert(0, opening);
                    argumentsBlock.Add(closing);

                    BracketsExpression arguments = MultipleStatementBlockParsing(argumentsBlock, out int len, opening, closing, context);
                    arguments.Actions.Reverse();
                    function = function.Clone();
                    foreach (var action in arguments.Actions)
                    {
                        function.AppendArgumentExpression(action);
                    }

                    i = i + len;
                    length = i;

                    return function;
                }
            }


            return null;
        }

        public BaseExpression Make(string code, Context context)
        {
            List<Token> tokens = Lexer.Lex(code);

            BaseExpression brackets;
            brackets = BracketsParsing(tokens, out int length, context);

            return brackets;
        }

        public BaseExpression Work(string code, Context context)
        {
            List<Token> tokens = Lexer.Lex(code);

            Token now = null;
            Token previous = null;

            BaseExpression expression = null;
            List<BaseExpression> elements = new List<BaseExpression>();

            for (int i = 0; i < tokens.Count(); i++)
            {
                if (i > 0)
                    previous = tokens[i-1];

                now = tokens[i];
                expression = Translate(now, context);
                if (now.Type == "keyword")
                {
                    if (now.Data == "if")
                    {
                        IfExpression exp = ParceIf(tokens.GetRange(i, tokens.Count() - i), out int length, context);
                       
                        if (exp != null)
                        {
                            i += length;
                            elements.Add(exp);
                            continue;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else if (now.Data == "while")
                    {
                        WhileExpression exp = ParceWhile(tokens.GetRange(i, tokens.Count() - i), out int length, context);

                        if (exp != null)
                        {
                            i += length;
                            elements.Add(exp);
                            continue;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else if (now.Data == "function")
                    {

                    }
                }
                else if (now.Type == "(")
                {
                    if (previous != null)
                    {
                        if (previous.Type == "keyword")
                        {
                            if (previous.Data == "function")
                            {
                                Console.WriteLine("stepped into an opening parenthesis in function arguments declaration");
                                continue;
                            }
                            else if (previous.Data == "if")
                            {
                                Console.WriteLine("stepped into an opening parenthesis after a if");
                                continue;
                            }
                            else if (previous.Data == "while")
                            {
                                Console.WriteLine("stepped into an opening parenthesis after a while");
                                continue;
                            }


                            Console.WriteLine("stepped into an opening parenthesis after a function");
                            //Console.ReadLine();
                            //return;
                            continue;
                        }
                        else if (previous.Type == "symbol")
                        {
                            Console.WriteLine("stepped into an opening parenthesis after a symbol");
                            //Console.ReadLine();
                            //return;
                            continue;
                        }
                        else
                        {
                            BaseExpression parenthesis;
                            parenthesis = ParenthesisParsing(tokens.GetRange(i, tokens.Count() - i), out int length, context);
                            // plus one to step on the closing parenthesis
                            // so in the next iteration tokens[i] will be on
                            // the token which is after closing parenthesis
                            i += length + 1;

                            if (parenthesis != null)
                                elements.Add(parenthesis);
                            continue;
                        }
                    }
                }
                else if (now.Type == ";")
                {
                    break;
                }
                else if (now.Type == "eof")
                {
                    break;
                }

                elements.Add(expression);
            }

            BaseExpression root = null;
            if (elements != null && elements.Count() > 0)
            {
                root = CreateTree(elements);

                if (root != null)
                {
                    //object result = root.Interpret(context);
                    //if (result is string)
                    //    Console.WriteLine("> \'" + (result ?? "null") + "\'");
                    //else
                    //    Console.WriteLine("> " + (result ?? "null"));
                }
            }
            return root;
        }

        private BaseExpression Translate(Token token, Context context)
        {
            BaseExpression expression = null;

            if (token.Type == "number")
            {
                expression = new NumberExpression(Convert.ToInt32(token.Data));
                expression.Priority = 0;
                expression.token = token;
            }
            else if (token.Type == "string")
            {
                expression = new StringExpression(Convert.ToString(token.Data));
                expression.Priority = 0;
                expression.token = token;
            }
            else if (token.Type == "logic")
            {
                if (token.Data == "and")
                {
                    expression = new AndExpression();
                    expression.Priority = 11;
                    expression.token = token;
                }
                else if (token.Data == "or")
                {
                    expression = new OrExpression();
                    expression.Priority = 12;
                    expression.token = token;
                }
                else if (token.Data == "xor")
                {
                    expression = new XorExpression();
                    expression.Priority = 12;
                    expression.token = token;
                }
            }
            else if (token.Type == "operation")
            {
                if (token.Data == "+")
                {
                    expression = new AddExpression();
                    expression.Priority = 4;
                    expression.token = token;
                }
                else if (token.Data == "-")
                {
                    expression = new SubstractExpression();
                    expression.Priority = 4;
                    expression.token = token;
                }
                else if (token.Data == "*")
                {
                    expression = new MultiplyExpression();
                    expression.Priority = 3;
                    expression.token = token;
                }
                else if (token.Data == "/")
                {
                    expression = new DivideExpression();
                    expression.Priority = 3;
                    expression.token = token;
                }
                else if (token.Data == "%")
                {
                    expression = new ModuleDivisionExpression();
                    expression.Priority = 3;
                    expression.token = token;
                }
            }
            else if (token.Type == "symbol")
            {
                expression = new VariableExpression();
                expression.Priority = 0;
                expression.token = token;

                if (context.ContainsKey(token.Data) == false)
                {
                    object a = new object();
                    a = "0";
                    context.CreateValue(token.Data, a);
                }
            }
            else if (token.Type == "=")
            {
                expression = new AssignExpression();
                expression.Priority = 11;
                expression.token = token;
            }
            else if (token.Type == "(")
            {
                expression = new ParenthesisExpression();
                expression.Priority = 7;
                expression.token = token;
            }
            else if (token.Type == ")")
            {
                expression = new ParenthesisExpression();
                expression.Priority = 7;
                expression.token = token;
            }
            else if (token.Type == "==")
            {
                expression = new EqualsExpression();
                expression.Priority = 7;
                expression.token = token;
            }
            else if (token.Type == ">")
            {
                expression = new GreaterThenExpression();
                expression.Priority = 6;
                expression.token = token;
            }
            else if (token.Type == "<")
            {
                expression = new LessThenExpression();
                expression.Priority = 6;
                expression.token = token;
            }
            else if (token.Type == ">=")
            {
                expression = new GEExpression();
                expression.Priority = 6;
                expression.token = token;
            }
            else if (token.Type == "<=")
            {
                expression = new LEExpression();
                expression.Priority = 6;
                expression.token = token;
            }
            else if (token.Type != "")
            {
                expression = new PunctuationExpression();
                expression.Priority = 0;
                expression.token = token;
            }
            else
            {
                return null;
            }

            return expression;
        }

        static private BaseExpression CheckTypeAndAdd(BaseExpression tree, BaseExpression node)
        {
            switch (tree)
            {
                case BinaryExpression binary:
                    if (binary.LeftOperand == null)
                    {
                        binary.LeftOperand = AddToTree(binary.RightOperand, node);
                    }
                    else
                    {
                        binary.RightOperand = AddToTree(binary.RightOperand, node);
                    }
                    return binary;

                //case UnaryExpression ex1:
                    //ex1.Operand1 = AddToTree(ex1.Operand1, node);
                    //return ex1;

                default:
                    return AddToTree(node, tree);
            }
        }

        static private BaseExpression AddToTree(BaseExpression tree, BaseExpression node)
        {
            if (tree == null)
            {
                return node;
            }

            if (tree.Priority > node.Priority)
            {
                return CheckTypeAndAdd(tree, node);
            }
            else if (tree.Priority == node.Priority)
            {
                return CheckTypeAndAdd(node, tree);
            }
            else
            {
                return AddToTree(node, tree);
            }

        }

        static private BaseExpression CreateTree(List<BaseExpression> expressions)
        {
            BaseExpression tree = null;

            foreach (var token in expressions)
            {
                tree = AddToTree(tree, token);
            }

            //if (tree != null)
            //{
            //    dynamic t = tree;
            //    Console.WriteLine("tree: \n" + t.ToString(true));
            //}

            return tree;
        }

        public BaseExpression ParenthesisParsing(List<Token> tokens, out int length, Context context)
        {
            Token opening = new Token("(", "", 0, 0);
            Token closing = new Token(")", "", 0, 0);

            return SingleStatementBlockParsing(tokens, out length, opening, closing, context);
        }

        public BaseExpression BracketsParsing(List<Token> tokens, out int length, Context context)
        {
            Token opening = new Token("{", "", 0, 0);
            Token closing = new Token("}", "", 0, 0);

            return MultipleStatementBlockParsing(tokens, out length, opening, closing, context);
        }

        public BaseExpression SingleStatementBlockParsing(List<Token> tokens, out int length, Token opening, Token closing, Context context)
        {
            BaseExpression expression = null;
            length = 0;

            Token alternative_opening = null;
            Token alternative_closing = null;
            if (opening.Type == "(" && closing.Type == ")")
            {
                alternative_opening = new Token("{", "", 0, 0);
                alternative_closing = new Token("}", "", 0, 0);
            }
            else if (opening.Type == "{" && closing.Type == "}")
            {
                alternative_opening = new Token("(", "", 0, 0);
                alternative_closing = new Token(")", "", 0, 0);
            }

            List<BaseExpression> elements = new List<BaseExpression>();
            if (tokens[0].Type == opening.Type)
            {
                List<Token> in_block = Lexer.BlockSequence(tokens, 0, opening, closing);
                length = in_block.Count();
                in_block.Add(closing);

                for (int i = 0; i < in_block.Count(); i++)
                {
                    if (in_block[i].Type == opening.Type)
                    {
                        List<Token> in_inner_block = Lexer.BlockSequence(in_block, i, opening, closing);
                        if (in_inner_block.Count() > 0)
                        {
                            in_inner_block.Insert(0, in_block[i]);
                            in_inner_block.Add(in_block[i + in_inner_block.Count()]);
                        }

                        BaseExpression result;
                        result = SingleStatementBlockParsing(in_inner_block, out int len, opening, closing, context);
                        if (result != null)
                        {
                            i += in_inner_block.Count() - 1;
                            elements.Add(result);
                        }

                    }
                    else if (in_block[i].Type == closing.Type)
                    {
                        expression = CreateTree(elements);

                        ParenthesisExpression parenthesis = new ParenthesisExpression();
                        parenthesis.Inside = expression;

                        return parenthesis;
                    }
                    else if (alternative_opening != null && in_block[i].Type == alternative_opening.Type)
                    {
                        List<Token> in_inner_block = Lexer.BlockSequence(in_block, i, alternative_opening, alternative_closing);
                        if (in_inner_block.Count() > 0)
                        {
                            in_inner_block.Insert(0, in_block[i]);
                            in_inner_block.Add(in_block[i + in_inner_block.Count()]);
                        }

                        BaseExpression result;
                        result = SingleStatementBlockParsing(in_inner_block, out int len, alternative_opening, alternative_closing, context);
                        if (result != null)
                        {
                            i += in_inner_block.Count() - 1;
                            elements.Add(result);
                        }
                    }
                    else
                    {
                        BaseExpression current = Translate(in_block[i], context);
                        if (current == null)
                            continue;
                        else
                            elements.Add(current);
                    }
                }
            }
            return expression;
        }

        public BracketsExpression MultipleStatementBlockParsing(List<Token> tokens, out int length, Token opening, Token closing, Context context)
        {
            BracketsExpression bracket = null;
            BaseExpression expression = null;
            length = 0;

            Token alternative_opening = null;
            Token alternative_closing = null;
            if (opening.Type == "(" && closing.Type == ")")
            {
                alternative_opening = new Token("{", "", 0, 0);
                alternative_closing = new Token("}", "", 0, 0);
            }
            else if (opening.Type == "{" && closing.Type == "}")
            {
                alternative_opening = new Token("(", "", 0, 0);
                alternative_closing = new Token(")", "", 0, 0);
            }

            // { 2 + 4; 3 - 1; }
            // 2 + 4; 3 - 1; }
            // 2 + 4; AppendAction;
            // 3 - 1; AppendAction;

            if (tokens[0].Type == opening.Type)
            {
                List<BaseExpression> elements = new List<BaseExpression>();
                bracket = new BracketsExpression();
                List<Token> in_block = Lexer.BlockSequence(tokens, 0, opening, closing);
                length = in_block.Count();

                in_block.Add(closing);
                //Lexer.DisplayCode(in_block);
                
                for (int i = 0; i < in_block.Count(); i++)
                {
                    if (in_block[i].Type == opening.Type)
                    {
                        List<Token> in_inner_block = Lexer.BlockSequence(in_block, i, opening, closing);
                        if (in_inner_block.Count() > 0)
                        {
                            in_inner_block.Insert(0, in_block[i]);
                            in_inner_block.Add(in_block[i + in_inner_block.Count()]);
                        }

                        BaseExpression result;
                        result = SingleStatementBlockParsing(in_inner_block, out int len, opening, closing, context);
                        if (result != null)
                        {
                            i += in_inner_block.Count() - 1;
                            elements.Add(result);
                        }

                    }
                    else if (in_block[i].Type == closing.Type)
                    {
                        return bracket;
                    }
                    else if (alternative_opening != null && in_block[i].Type == alternative_opening.Type)
                    {
                        List<Token> in_inner_block = Lexer.BlockSequence(in_block, i, alternative_opening, alternative_closing);
                        if (in_inner_block.Count() > 0)
                        {
                            in_inner_block.Insert(0, in_block[i]);
                            in_inner_block.Add(in_block[i + in_inner_block.Count()]);
                        }

                        BaseExpression result;
                        result = SingleStatementBlockParsing(in_inner_block, out int len, alternative_opening, alternative_closing, context);
                        if (result != null)
                        {
                            i += in_inner_block.Count() - 1;
                            elements.Add(result);
                        }
                    }
                    else if (in_block[i].Type == "keyword")
                    {
                        if (in_block[i].Data == "if")
                        {
                            IfExpression ifExp = ParceIf(in_block.GetRange(i, in_block.Count() - i), out int len, context);
                            i += len;
                            if (ifExp != null)
                            {
                                bracket.AppendAction(ifExp);
                                elements.Clear();
                                continue;
                            }
                        }
                        else if (in_block[i].Data == "while")
                        {
                            WhileExpression whileExp = ParceWhile(in_block.GetRange(i, in_block.Count() - i), out int len, context);
                            i += len;
                            if (whileExp != null)
                            {
                                bracket.AppendAction(whileExp);
                                elements.Clear();
                                continue;
                            }
                        }
                        else if (in_block[i].Data == "function")
                        {
                            //object definition;
                            //definition = context.GetValue(in_block[i + 1].Data);
                            //if (definition is FunctionDefinitionExpression)
                            //{
                            //    //FunctionExpression function;
                            //    //function= new FunctionExpression((FunctionDefinitionExpression)definition);

                            //    i += 2;
                            //    elements.Add((FunctionDefinitionExpression)definition);
                            //    continue;
                            //}

                            FunctionDefinitionExpression functionDefinition;
                            functionDefinition = ParseFunctionDefinition(in_block.GetRange(i, in_block.Count() - i), out int len, context);

                            context.AddValue(in_block[i + 1].Data, functionDefinition);

                            i += len;
                            if (functionDefinition != null)
                            {
                                // to step over "}"
                                i = i + 1;

                                elements.Add(functionDefinition);

                                BaseExpression root = CreateTree(elements);
                                bracket.AppendAction(root);
                                elements.Clear();
                                continue;
                            }

                            //if (in_block[i + 3].Type != "(")
                            //{
                            //    i += 2;
                            //    elements.Add(new FunctionExpression());
                            //    continue;
                            //}
                            //else
                            //{
                            //    i += 2;
                            //    continue;
                            //}
                        }
                        else if (in_block[i].Data == "call")
                        {
                            if (in_block[i + 2].Type == "symbol")
                            {
                                FunctionExpression function;
                                if (in_block[i + 3].Type == "(")
                                {
                                    function = ParseFunctionCall(in_block.GetRange(i, in_block.Count() - i), out int len, context);
                                    if (function.Function != null) ;
                                    else
                                        function.Function = (FunctionDefinitionExpression)context.GetValue(in_block[i + 2].Data);


                                    i += len;

                                    if (function != null)
                                    {
                                        // to step over ")"
                                        //i = i + 1;

                                        elements.Add(function);
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                    else if (in_block[i].Type == ";")
                    {
                        if (elements.Count() > 0)
                        {
                            BaseExpression root = CreateTree(elements);
                            bracket.AppendAction(root);
                            elements.Clear();
                        }
                    }
                    else
                    {
                        if (in_block[i].Type == "symbol")
                        {
                            if (context.ContainsKey(in_block[i].Data))
                            {
                                dynamic result = context.GetValue(in_block[i].Data);
                                if (result as FunctionExpression != null || result as FunctionDefinitionExpression != null)
                                {
                                    if (in_block[i + 1].Type != "(")
                                    {
                                        if (result != null)
                                        {
                                            if (result as FunctionDefinitionExpression != null)
                                                elements.Add(result);
                                            else
                                                elements.Add(result.Function);
                                        }
                                        continue;
                                    }
                                    FunctionExpression function;
                                    //then this is a function call
                                    function = ParseFunctionCall(in_block.GetRange(i, in_block.Count() - i), out int len, context);
                                    if (function.Function != null);
                                    else
                                        function.Function = (FunctionDefinitionExpression)context.GetValue(in_block[i].Data);
                                    i += len;

                                    if (function != null)
                                    {
                                        // to step over ")"
                                        i = i + 1;
                                        elements.Add(function);
                                        continue;
                                    }
                                }
                            }
                        }

                        BaseExpression current = Translate(in_block[i], context);
                        if (current == null)
                        {
                            continue;
                        }
                        else
                        {
                            elements.Add(current);
                        }
                    }
                }
            }
            return bracket;
        }
    }
}
