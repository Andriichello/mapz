﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER
{
    public class Token
    {
        public Token(string type, string data, int row, int col)
        {
            this.mType = type;
            this.mData = data;

            this.mRow = row;
            this.mCol = col;
        }


        public static string ToString(Token token)
        {
            string output = "";
            output += string.Format("[r: {0, 3}, c: {1, 3}] ", token.Row, token.Col);
            output += string.Format("('{0}' ,  '{1}')", token.Type, token.Data);

            //output += "[" + "r: " + token.Row + " , " + "c: " + token.Col + "]";
            //output += "(" + "\"" + token.Type + "\"" + " , " + "\"" + token.Data + "\""+ ")";

            return output;
        }

        private string mType;
        private string mData;
        public string Type { get { return mType; } }
        public string Data { get { return mData; } }

        private int mRow;
        private int mCol;
        public int Row { get { return mRow; } }
        public int Col { get { return mCol; } }
    }
}
