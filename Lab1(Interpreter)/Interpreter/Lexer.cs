﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER
{
	// BNF
	//<nonzero> ::= 1 | 2 | ... | 9
	//<character> ::= _ | a | b | ... | z | A | B | ... | Z
	//<letter> ::= <character> | <number>	
	//<string> ::= <letter> | <letter><string>
	//<number> ::= 0 | <nonzero><number>
	//<symbol> ::= <character> | <character><symbol> | <character><number> | <character><number><symbol> 


	//<operator > ::= - | + | * | / | %
	//<operand> ::= <symbol> | <number> 
	//<operation> ::= <operand><operator><operand> | <operand><operator><operation>
	//<assignment> ::= <symbol> = <operand> | <symbol> = <operation>
	//<condition> ::= ( <operand> and<operand> ) | ( <operand> or<operand> ) | ( <operand> xor<operand> )
	//<function-definition-arguments> ::= <empty> | <symbol> | <symbol>;<function-definition-arguments>
	//<block> ::= { <statements> }

	//<function-declaration> ::= <symbol> = function(<function-definition-arguments>) <block>; | function<symbol>(<function-definition-arguments>) <block>;

	//<function-call-arguments> ::=  ::= <empty> | <symbol> | <number> | <symbol>;<function-call-arguments> | <number>;<function-call-arguments> 

	//<function-call> ::= <symbol>(<function-call-arguments>);

	//<if> ::= if <condition> <block>	
	//<while> ::= while <condition> <block>
	//<statement> ::= <assignment>; | <operation>; | <if> | <while>
	//<statements> ::= <statement> | <statement><statements>

	public class Lexer	
    {
        public Lexer(string code)
        {
            this.mCode = code;
            this.mTokens = Lex(code);
        }

        public Lexer(List<Token> tokens)
        {
            this.mTokens = tokens;
        }

        static public List<Token> Lex(string input)
        {
            List<Token> tokens = new List<Token>();

			if (input.Count() == 0)
				return tokens;

			int last_newline = 0;
			int line_count = 0;
			int column = 0;

			char current = input.ElementAt(0);
			for (int i = 0; i < input.Count(); ++i)
			{
				column = i;
				current = input.ElementAt(i);

				if (current == '\n')
				{
					int from = last_newline;
					int to = i;

					//mCodeLines.Add(mCode.Substring(from, to - from));
					last_newline = i;
					
					line_count++;
				}

				if (my_operation(current) == true)
				{
					string value = "";
					value += current;

					tokens.Add(new Token("operation", value, line_count, column - last_newline));
				}
				else if (my_other(current) == true)
				{
					string value = "";
					value += current;
					if ((i + 1) < input.Count() && input.ElementAt(i + 1) == '=')
					{
						if (current == '=' || current == '>' || current == '<')
						{
							value += '=';
							i++;
						}
					}

					tokens.Add(new Token(value, value, line_count, column - last_newline));
				}
				else if ('\"' == current)
				{
					++i;// getting over opening " 
					string value = "";
					int length = CountString(input, i);
					value = input.Substring(i, length);
					 
					tokens.Add(new Token("string", value, line_count, column - last_newline));
					i += length;
				}
				else if (my_number(current))
				{
					string value = "";
					int length = CountNumber(input, i);
					value = input.Substring(i, length);

					tokens.Add(new Token("number", value, line_count, column - last_newline));
					i += length - 1;
				}
				else if (my_symbol(current))
				{
					string value = "";
					int length = CountSymbol(input, i);
					value = input.Substring(i, length);
					tokens.Add(new Token("symbol", value, line_count, column - last_newline));
				
					i += length - 1;
				}

				if (tokens.Count() > 0)
				{
					Token revised = ReviseToken(tokens.Last());
					if (tokens.ElementAt(tokens.Count() - 1) != revised)
					{
						tokens.RemoveAt(tokens.Count() - 1);
						tokens.Add(revised);
					}
				}
				column = i;
			}
			tokens.Add(new Token("eof", "", line_count, column - last_newline));

			return tokens;
        }

        static bool my_number(char ch)
        {
			string numbers = "0123456789";
			return numbers.Contains(ch);
        }

        static bool my_symbol(char ch)
        {
			if (ch == '_')
				return true;
			if (ch >= 'a' && ch <= 'z')
				return true;
			if (ch >= 'A' && ch <= 'Z')
				return true;
			return false;
        }

		static bool my_operation(char ch)
		{
			string operations = "+-/*%";
			return operations.Contains(ch);
		}

		static bool my_other(char ch)
		{
			string other = "{}(),;:=<>";
			return other.Contains(ch);
		}

		static int CountNumber(string input, int position)
		{
			int length = 0;
			for (int i = position; i < input.Count() + position; i++)
			{
				if (my_number(input.ElementAt(i)) == false &&
					input.ElementAt(i) != '.')
					break;
				length++;
			}
			return length;
		}

		static int CountSymbol(string input, int position)
		{
			int length = 0;
			for (int i = position; i < input.Count() + position; i++)
			{
				if (my_symbol(input.ElementAt(i)) == false)
					break;
				length++;
			}
			return length;
		}

		static int CountString(string input, int position)
		{
			int length = 0;
			for (int i = position; i < input.Count() + position; i++)
			{
				if (input.ElementAt(i) == '\"')
					break;
				length++;
			}

			return length;
		}

		static private Token ReviseToken(Token token)
		{
			if (token.Data.Equals("if"))
			{
				return new Token("keyword", "if", token.Row, token.Col);
			}
			else if (token.Data.Equals("while"))
			{
				return new Token("keyword", "while", token.Row, token.Col);
			}
			else if (token.Data.Equals("function"))
			{
				return new Token("keyword", "function", token.Row, token.Col);
			}
			else if (token.Data.Equals("call"))
			{
				return new Token("keyword", "call", token.Row, token.Col);
			}
			else if (token.Data.Equals("and"))
			{
				return new Token("logic", "and", token.Row, token.Col);
			}
			else if (token.Data.Equals("or"))
			{
				return new Token("logic", "or", token.Row, token.Col);
			}
			else if (token.Data.Equals("xor"))
			{
				return new Token("logic", "xor", token.Row, token.Col);
			}
			return token;
		}

		static public string StringBlockSequence(string input, ref int position, char opening, char closing)
		{
			string sequence = "";
			int openings = 0;

			for (int i = position; i < input.Count(); i++)
			{
				position = -1;
				if (input[i] == opening)
				{
					++openings;
					position = i;
					break;
				}
			}


			for (int i = position; i < input.Count(); i++)
			{
				while (openings > 0)
				{
					sequence += input[i];
					++i;
					if (i >= input.Count())
						break; // display error message

					if (input[i] == opening)
					{
						++openings;
					}
					else if (input[i] == closing)
					{
						--openings;
						if (openings == 0)
						{
							sequence += input[i];
							break; // move to recursion
						}
					}
				}
			}

			return sequence;
		}
	
		static public List<Token> BlockSequence(List<Token> tokens, int position, Token opening, Token closing)
		{
			List<Token> sequence = new List<Token>();
			int openings = 0;

			for (int i = position; i < tokens.Count(); i++)
			{
				if (tokens[i].Type == opening.Type)
				{
					++openings;
					position = i;
					break;
				}
			}

			for (int i = position; i < tokens.Count(); i++)
			{
				while (openings > 0)
				{
					sequence.Add(tokens[i]);
					++i;
					if (i >= tokens.Count())
						break; // display error message

					if (tokens[i].Type == opening.Type)
					{
						++openings;
					}
					else if (tokens[i].Type == closing.Type)
					{
						--openings;
						if (openings == 0)
						{
							sequence.RemoveAt(0);
							//sequence.Add(tokens[i]);
							return sequence;
						}
					}
				}
			}
			return sequence;
		}

		static public List<Token> GetInnerBlock(List<Token> tokens, int position, Token opening, Token closing)
		{
			List<Token> inner = new List<Token>();
			inner = BlockSequence(tokens, position, opening, closing);

			return inner;
		}

		public void Display()
        {
			//Console.WriteLine("Lines: ");
			//for (int i = 0; i < mCodeLines.Count(); i++)
			//{
			//	Console.WriteLine(mCodeLines.ElementAt(i));
			//}
			Console.WriteLine("Tokens: ");
			for (int i = 0; i < mTokens.Count(); i++)
			{
				Console.WriteLine(Token.ToString(mTokens.ElementAt(i)));
			}
			Console.WriteLine();
		}

		static public string Display(List<Token> tokens)
		{
			//Console.WriteLine("Lines: ");
			//for (int i = 0; i < mCodeLines.Count(); i++)
			//{
			//	Console.WriteLine(mCodeLines.ElementAt(i));
			//}
			string output = "";
			output += "Tokens: \n";

			//Console.WriteLine("Tokens: ");
			for (int i = 0; i < tokens.Count(); i++)
			{
				output += Token.ToString(tokens.ElementAt(i)) + "\n";
			}
			Console.WriteLine(output);
			return output;
		}

		static public string DisplayCode(List<Token> tokens)
		{
			//Console.WriteLine("Lines: ");
			//for (int i = 0; i < mCodeLines.Count(); i++)
			//{
			//	Console.WriteLine(mCodeLines.ElementAt(i));
			//}
			string output = "";
			output += "Code: \n";

			//Console.WriteLine("Tokens: ");
			for (int i = 0; i < tokens.Count(); i++)
			{
				if (tokens[i].Type == "eof")
					break;
				output += tokens[i].Data;
			}
			Console.WriteLine(output);
			return output;
		}

		private string mCode;
        public string Code { get { return this.mCode; } }

		private List<Token> mTokens;
        public List<Token> Tokens { get { return this.mTokens; } }
    }

	

}
