﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER
{
    public class Context
    {
        private static Dictionary<string, object> mGlobalDictionary = new Dictionary<string, object>();
        private static bool isInitiated = false;

        private Dictionary<string, object> mDictionary;
        public Dictionary<string, object> dictionary
        {
            get { return mDictionary; }
        }

        public Context()    
        {
            mDictionary = new Dictionary<string, object>();
            if (isInitiated == false)
            {
                isInitiated = true;
                mGlobalDictionary.Add("regexp", new RegexpFunction());
                mGlobalDictionary.Add("regpos", new RegexpPositionFunction());
                mGlobalDictionary.Add("substr", new SubstrFunction());
                mGlobalDictionary.Add("print", new PrintFunction());
                mGlobalDictionary.Add("sentence", "hello world of programmers.");
            }
        }

        public bool ContainsKey(string key)
        {
            if (mDictionary.ContainsKey(key) || mGlobalDictionary.ContainsKey(key))
                return true;
            return false;
        }

        public object GetValue(string name)
        {
            object value;
            if (mDictionary.TryGetValue(name, out value))
            {
                return value;
            }
            else
            {
                if (mGlobalDictionary.TryGetValue(name, out value))
                {
                    return value;
                }
                // there is no such key in mDictionary
                return 0;
            }
        }

        public void AddValue(string name, object value)
        {
            if (mDictionary.ContainsKey(name))
            {
                //mDictionary[name]
                mDictionary[name] = value;
            }
            else
                mDictionary.Add(name, value);
        }

        public void CreateValue(string name, object value)
        {
            if (mDictionary.ContainsKey(name))
                return;
            else
                mDictionary.Add(name, value);
        }

    }
}
