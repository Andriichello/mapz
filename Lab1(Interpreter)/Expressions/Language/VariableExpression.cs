﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER
{
    public class VariableExpression : BaseExpression
    {
        public VariableExpression() : base(0)
        {

        }

        public override object Interpret(Context context) 
        {
            object a = context.GetValue(this.token.Data);

            /*
            if (a is string)
            {
                return Convert.ToString(a);
            }
            if (a is int)
            {
                return Convert.ToInt32(a);
            }
            */
            if (a is FunctionDefinitionExpression)
            {
                //return "FunctionDefinitionExpression";
            }

            return a;
        }

        public override string ToString(bool what) { return token.Data; }
    }
}
