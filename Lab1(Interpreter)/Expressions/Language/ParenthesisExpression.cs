﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTERPRETER
{
    class ParenthesisExpression : BaseExpression
    {
        public ParenthesisExpression() : base(0)
        {

        }

        public override object Interpret(Context context)
        {
            object a = mInside.Interpret(context);
            
            return a;
        }

        public override string ToString(bool what)
        {

            if (Inside == null)
                return "()";

            dynamic t = Inside;
            return "()" + "\n" + t.ToString(true);
        }

        new public TreeNode ToTreeNode()
        {
            TreeNode node = new TreeNode(this.GetType().Name);
            if (Inside != null)
            {
                node.Nodes.Add(((dynamic)Inside).ToTreeNode());
            }

            return node;
        }

        private BaseExpression mInside;
        public BaseExpression Inside
        {
            get { return mInside; }
            set { mInside = value; }
        }
        


    }
}
