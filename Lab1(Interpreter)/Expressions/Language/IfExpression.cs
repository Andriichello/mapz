﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTERPRETER
{
    public class IfExpression : BaseExpression
    {
        public IfExpression() : base(0)
        {

        }

        public override object Interpret(Context context)
        {
            object condition = Condition.Interpret(context);
            object block = null;

            bool v = false;
            if (condition is string)
            {
                v = (Convert.ToString(condition).ToLower()).Contains("true");
            }
            if (condition is int)
            {
                v = Convert.ToBoolean(condition);
            }
            if (condition is bool)
            {
                v = Convert.ToBoolean(condition);
            }

            if (v == true)
            {
                block = Block.Interpret(context);
            }

            return block;
        }

        public override string ToString(bool what)
        {
            return "IfExpression";
        }

        new public TreeNode ToTreeNode()
        {
            TreeNode node = new TreeNode(this.GetType().Name);
            TreeNode condition = new TreeNode("Condition");
            if (Condition != null)
            {
                condition.Nodes.Add(((dynamic)Condition).ToTreeNode());
                node.Nodes.Add(condition);
            }

            if (Block != null)
            {
                node.Nodes.Add(((dynamic)Block).ToTreeNode());
            }

            return node;
        }

        private BaseExpression mCondition;
        public BaseExpression Condition
        {
            get { return mCondition; }
            set { mCondition = value; }
        }

        private BaseExpression mBlock;
        public BaseExpression Block
        {
            get { return mBlock; }
            set { mBlock = value; }
        }
    }
}
