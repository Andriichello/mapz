﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER
{
    class StringExpression : BaseExpression
    {
        public StringExpression(string value = "") : base(0)
        {
            this.mData = value;
        }

        public override object Interpret(Context context)
        {
            return this.mData;
        }

        public override string ToString(bool what)
        {
            return this.Data;
        }

        private string mData;
        public string Data
        {
            get { return mData; }
            set { mData = value; }
        }

    }
}
