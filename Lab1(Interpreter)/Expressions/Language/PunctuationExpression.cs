﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER
{
    public class PunctuationExpression : BaseExpression
    {
        public PunctuationExpression() : base(0)
        {

        }

        public override object Interpret(Context context)
        {

            return base.Interpret(context);
        }

        public override string ToString(bool what)
        {
            return "PunctuationExpression";
        }
    }
}
