﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTERPRETER
{
    public class BracketsExpression : BaseExpression
    {
        public BracketsExpression() : base(0)
        {
            mActions = new List<BaseExpression>();
        }

        public override object Interpret(Context context)
        {
            object a = null;
            int count = 0;
            foreach (var action in mActions)
            {
                count++;
                if (action != null)
                {
                    a = action.Interpret(context);
                    //Console.WriteLine(count + ". " + a);
                }
            }
            
            return a;
        }

        new public void Optimize()
        {
            if (mActions == null)
                return;
            for (int i = 0; i < mActions.Count; i++)
            {
                dynamic action = mActions[i];
                if (action as BinaryExpression != null)
                {
                    if (action as AssignExpression != null)
                        continue;
                    mActions.RemoveAt(i);
                    i--;
                    continue;
                }
                else if (action as IfExpression != null || action as WhileExpression != null)
                {
                    if (action.Block == null || action.Condition == null)
                    {
                        mActions.RemoveAt(i);
                        i--;
                        continue;
                    }
                    else
                    {
                        dynamic block = action.Block;
                        if (block as BracketsExpression != null)
                        {
                            block.Optimize();
                            if (block.mActions != null && block.mActions.Count > 0);
                            else
                            {
                                mActions.RemoveAt(i);
                                i--;
                                continue;
                            }
                        }
                    }
                }
                else if (action as FunctionDefinitionExpression != null)
                {
                    if (action.Block == null)
                    {
                        mActions.RemoveAt(i);
                        i--;
                        continue;
                    }
                    else
                    {
                        dynamic block = action.Block;
                        if (block as BracketsExpression != null)
                        {
                            block.Optimize();
                            if (block.mActions != null && block.mActions.Count > 0);
                            else
                            {
                                mActions.RemoveAt(i);
                                i--;
                                continue;
                            }
                        }
                    }
                }
            }
        }


        public override string ToString(bool what)
        {
            string output = "{}" + "\n";

            foreach (dynamic t in Actions)
            {
                
                if (t as BinaryExpression != null)
                    output += t.ToTreeString("") + "\n";
                else
                   output += t.ToString(true) + "\n";
            }

            return output;
        }

        new public TreeNode ToTreeNode()
        {
            //TreeNode brackets = new TreeNode("{}");
            TreeNode brackets = new TreeNode(this.GetType().Name);

            foreach (dynamic action in mActions)
            {
                if (action == null)
                    continue;

                TreeNode node = action.ToTreeNode();
                brackets.Nodes.Add(node);
            }
            return brackets;
        }

        private List<BaseExpression> mActions;

        public List<BaseExpression> Actions
        {
            get { return mActions; }
        }

        public void AppendAction(BaseExpression expression)
        {
            mActions.Add(expression);
        }
    }
}
