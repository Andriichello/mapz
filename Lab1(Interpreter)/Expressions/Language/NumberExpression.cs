﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER
{
    public class NumberExpression : BaseExpression
    {
        public NumberExpression(int value = 0) : base(0)
        {
            this.mData = value;
        }

        public override object Interpret(Context context)
        {
            return this.mData;
        }

        public override string ToString(bool what)
        {
            return token.Data; 
        }

        private int mData;
        public int Data
        {
            get{ return mData; }
            set{ mData = value; }
        }

    }
}
