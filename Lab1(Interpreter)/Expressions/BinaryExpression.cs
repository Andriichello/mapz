﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTERPRETER
{
    public abstract class BinaryExpression : BaseExpression
    {
        public BinaryExpression(BaseExpression left = null, BaseExpression right = null)
            : base(0)
        {
            this.mLeftOperand = left;
            this.mRightOperand = right;
        }

        public override object Interpret(Context context) 
        {
            return null;
        }
        
        new public virtual string ToString(bool what) {
            dynamic left = mLeftOperand;
            dynamic right = mRightOperand;
            return "binary" + "\n" + mLeftOperand.ToString(true) +
                "\t" + mRightOperand.ToString(true);

            return string.Format("binary: \n\t1. {0}\n\t2. {1}",
                mLeftOperand.ToString(true) ?? "null",
                mRightOperand.ToString(true) ?? "null");
        }

        public string ToTreeString(string marginString)
        {
            string output = "";
            if (mToken != null)
                output += mToken.Data;
            else
                output += "binary";

            dynamic left = mLeftOperand;
            dynamic right = mRightOperand;
            output += "\n";
            output += marginString + "left: ";
            if (left as BinaryExpression != null)
                output += marginString + left.ToTreeString(marginString + "\t");
            else
                output += marginString + left.ToString(true);

            output += "\n";
            output += marginString + "right: ";
            if (right as BinaryExpression != null)
                output += marginString + right.ToTreeString(marginString + "\t");
            else
                output += marginString + right.ToString(true);
            return output;
        }

        new public TreeNode ToTreeNode()
        {
            TreeNode node = new TreeNode(this.GetType().Name);
            dynamic left = mLeftOperand;
            dynamic right = mRightOperand;
            if (left != null)
                node.Nodes.Add(left.ToTreeNode());
            if (right != null)
                node.Nodes.Add(right.ToTreeNode());

            return node;

            //if (token != null)
            //{
            //    TreeNode node = new TreeNode(token.Type);
            //    dynamic left = mLeftOperand;
            //    dynamic right = mRightOperand;
            //    if (left != null)
            //        node.Nodes.Add(left.ToTreeNode());
            //    if (right != null)
            //        node.Nodes.Add(right.ToTreeNode());
                
            //    return node;
            //}
            //else
            //    return new TreeNode("BinaryExpression");
        }

        protected BaseExpression mLeftOperand;
        protected BaseExpression mRightOperand;

        public BaseExpression LeftOperand
        {
            get { return mLeftOperand; }
            set { mLeftOperand = value; }
        }
        public BaseExpression RightOperand
        {
            get { return mRightOperand; }
            set { mRightOperand = value; }
        }
    }
}
