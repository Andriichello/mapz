﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTERPRETER
{
    public class FunctionExpression : BaseExpression
    {
        public FunctionExpression(FunctionDefinitionExpression function = null) : base(0)
        {
            Function = function;
            this.mArgumentsExpressions = new List<BaseExpression>();
        }

        public FunctionExpression Clone()
        {
            FunctionExpression fExpression = (FunctionExpression)MemberwiseClone();
            if (fExpression.Function != null)
            {
                fExpression.mArgumentsExpressions = new List<BaseExpression>();
            }
            return fExpression;
        }

        public override object Interpret(Context context)
        {
            object a = null;

            // changing values of function arguments if they are passed
            // it won't be a problem if more arguments passed
            //Context replaced = new Context();
            //foreach (var line in context.dictionary)
            //{
            //    replaced.AddValue(line.Key, line.Value);
            //}

            if (Function == null)
                return null;

            for (int i = 0; i < Function.Arguments.Count; i++)
            {                               
                string key = Function.Arguments[i].Key;
                object value = Function.Arguments[i].Value;
                // calculates the expression and assignes it's
                // result to a context dictionary as value to a
                // an argument name(key)

                if (i < mArgumentsExpressions.Count)
                {
                    value = mArgumentsExpressions[i].Interpret(context);
                }
                
                Function.LocalContext.AddValue(key, value);
                //replaced.AddValue(key, value);
            }
            //mArgumentsExpressions.Clear();

            if (Function.Block == null)
                return a;

            foreach (var action in Function.Block.Actions)
            {
                a = action.Interpret(Function.LocalContext);
            }

            return a;
        }

        public override string ToString(bool what)
        {
            string output = "FunctionExpression\n";
            output += "args: \n";
            int count = 0;
            foreach (dynamic argument in mArgumentsExpressions)
            {
                if (count < Function.Arguments.Count)
                {
                    output += "\t" + Function.Arguments.ElementAt(count).Key + " = ";
                }
                count++;
                if (argument as BinaryExpression != null)
                    output += argument.ToTreeString("\t\t") + "\n";
                else
                    output += argument.ToString(true) + "\n";
            }
            output += "\n";
            output += "actions: \n";
            count = 0;
            foreach (dynamic t in Function.Block.Actions)
            {
                count++;
                output += "\t" + count + ". ";
                if (t as BinaryExpression != null)
                    output += t.ToTreeString("\t\t") + "\n";
                else
                    output += t.ToString(true) + "\n";
            }

            return output;
            //return "FunctionExpression";
        }

        new public TreeNode ToTreeNode()
        {
            TreeNode node = new TreeNode(this.GetType().Name);
            TreeNode args = new TreeNode("Arguments");
            TreeNode block = new TreeNode("Block");

            foreach (dynamic argument in mArgumentsExpressions)
            {
                if (argument != null)
                    args.Nodes.Add(argument.ToTreeNode());
            }
            if (args.Nodes.Count > 0)
                node.Nodes.Add(args);

            if (Function != null && Function.Block != null)
            {
                foreach (dynamic action in Function.Block.Actions)
                {
                    if (action != null)
                        block.Nodes.Add(action.ToTreeNode());
                }
            }
            if (block.Nodes.Count > 0)
                node.Nodes.Add(block);

            return node;
        }

        protected List<BaseExpression> mArgumentsExpressions;
        public void AppendArgumentExpression(BaseExpression argument)
        {
            mArgumentsExpressions.Insert(0, argument);
            while (mArgumentsExpressions.Count() > Function.Arguments.Count())
            {
                mArgumentsExpressions.RemoveAt(mArgumentsExpressions.Count() - 1);
            }
        }

        protected FunctionDefinitionExpression mFunction;
        public FunctionDefinitionExpression Function
        {
            get { return this.mFunction; }
            set { this.mFunction = value; }
        }
    }


}
