﻿using INTERPRETER_WITH_UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER
{
    class PrintFunction : FunctionExpression
    {
        public PrintFunction(string output = "") : base(new FunctionDefinitionExpression())
        {
            base.Function.AppendArgument("output", null);
        }

        public override object Interpret(Context context)
        {
            // this gets the arguments for us
            base.Interpret(context);

            string output;
            object a;
            a = Function.LocalContext.GetValue("output");

            if (a != null)
            {
                output = Convert.ToString(a);
                Program.mainForm.outputBox.AppendText(output + Environment.NewLine);
                return output;
            }
            else
            {
                Program.mainForm.outputBox.AppendText("null" + Environment.NewLine);
                return null;

            }
        }

        public override string ToString(bool what)
        {
            return "PrintFunction";
        }

    }
}
