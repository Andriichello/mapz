﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace INTERPRETER
{
    public class RegexpFunction : FunctionExpression
    {
        public RegexpFunction(string pattern = "*") : base(new FunctionDefinitionExpression())
        {
            base.Function.AppendArgument("text", null);
            base.Function.AppendArgument("pattern", pattern);
        }

        public override object Interpret(Context context)
        {
            // this gets the arguments for us
            base.Interpret(context);

            string text; string pattern;
            object a; object b;
            a = Function.LocalContext.GetValue("text");
            b = Function.LocalContext.GetValue("pattern");

            if (a != null && b != null)
            {
                text = Convert.ToString(a);
                pattern = Convert.ToString(b);

                Regex regex = new Regex(pattern);
                Match match = regex.Match(text);
                if (match.Length > 0)
                {
                    return match.ToString();
                }
                else
                {
                    return null;
                }
            }
            else
                return null;
        }

        public override string ToString(bool what)
        {
            return "RegexpFunction";
        }

    }
}
