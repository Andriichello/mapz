﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace INTERPRETER
{
    class RegexpPositionFunction : FunctionExpression
    {
        public RegexpPositionFunction(string pattern = "*") : base(new FunctionDefinitionExpression())
        {
            base.Function.AppendArgument("text", null);
            base.Function.AppendArgument("pattern", pattern);
            base.Function.AppendArgument("index", null);
        }

        public override object Interpret(Context context)
        {
            // this gets the arguments for us
            base.Interpret(context);

            string text; string pattern; int index = 0;
            object a; object b; object c;
            a = Function.LocalContext.GetValue("text");
            b = Function.LocalContext.GetValue("pattern");
            c = Function.LocalContext.GetValue("index");

            if (a != null && b != null)
            {
                text = Convert.ToString(a);
                pattern = Convert.ToString(b);
                if (c != null)
                {
                    index = Convert.ToInt32(c);
                    if (index > 0 || index < text.Length)
                        text = text.Substring(index, text.Length - index);
                }

                Regex regex = new Regex(pattern);
                Match match = regex.Match(text);
                if (match.Length > 0)
                {
                    return index + match.Index;
                }
                else
                {
                    return -1;
                }
            }
            else
                return -1;
        }

        public override string ToString(bool what)
        {
            return "RegexpPositionFunction";
        }
    }
}
