﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTERPRETER
{
    public class FunctionDefinitionExpression : BaseExpression
    {
        public FunctionDefinitionExpression() : base(0)
        {
            LocalContext = new Context();
            mArgumentsList = new List<KeyValuePair<string, object>>();
        }

        public override object Interpret(Context context)
        {
            object a = this;
            return a;
        }

        public override string ToString(bool what)
        {
            string output = "FunctionDefinition\n";
            output += "args: ";
            for (int i = 0; i < Arguments.Count; i++)
            {
                dynamic argument = Arguments.ElementAt(i);
                output += argument.Key;
                if (i != Arguments.Count - 1)
                    output += ", ";
            }
            output += "\n";
            output += "actions: \n";
            int count = 0;
            foreach (dynamic t in Block.Actions)
            {
                count++;
                output += count + ". ";
                if (t as BinaryExpression != null)
                    output += t.ToTreeString("\t") + "\n";
                else
                    output += t.ToString(true) + "\n";
            }

            return output;
        }

        new public TreeNode ToTreeNode()
        {
            TreeNode node = new TreeNode(this.GetType().Name);
            TreeNode block = new TreeNode("Block");

            if (Block != null)
            {
                foreach (dynamic action in Block.Actions)
                {
                    if (action != null)
                        block.Nodes.Add(action.ToTreeNode());
                }
            }
            if (block.Nodes.Count > 0)
                node.Nodes.Add(block);

            return node;
        }

        // there can't be predifined so that's why 
        // in FunctionExpression class we will set
        // the value field of ArgumentsContext
        // and here we have only the keys
        protected List<KeyValuePair<string, object>> mArgumentsList;
        public List<KeyValuePair<string, object>> Arguments
        {
            get { return this.mArgumentsList; }
        }

        // "a", "b", "c", ...
        public void AppendArgument(string name, object default_value)
        {
            Arguments.Add(new KeyValuePair<string, object>(name, default_value));
            LocalContext.AddValue(name, default_value);
        }

        // there are predefined actions in function body
        // that's why this is the version of block that
        // will be executed
        protected BracketsExpression mBlock;
        public BracketsExpression Block
        {
            get { return this.mBlock; }
            set { this.mBlock = value; }
        }

        // 2 + 4; x = 10; ...
        public void AppendAction(BaseExpression action)
        {
            this.mBlock.AppendAction(action);
        }

        public Context LocalContext;

    }
}
