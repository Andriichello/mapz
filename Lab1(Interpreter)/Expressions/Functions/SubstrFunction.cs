﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER
{
    class SubstrFunction : FunctionExpression
    {
        public SubstrFunction() : base(new FunctionDefinitionExpression())
        {
            base.Function.AppendArgument("string", null);
            base.Function.AppendArgument("index", null);
            base.Function.AppendArgument("length", null);
        }

        public override object Interpret(Context context)
        {
            // this gets the arguments for us
            base.Interpret(context);

            string str; int index; int lenght;
            object a; object b; object c;
            a = Function.LocalContext.GetValue("string");
            b = Function.LocalContext.GetValue("index");
            c = Function.LocalContext.GetValue("length");

            if (a != null && b != null)
            {
                str = Convert.ToString(a);
                index = Convert.ToInt32(b);
                if (c != null)
                {
                    lenght = Convert.ToInt32(c);
                    if (lenght > 0)
                    {
                        if (str.Length - index > lenght)
                            return str.Substring(index, lenght);
                    }
                }
                return str.Substring(index, str.Length - index);
            }
            return "";
        }

        public override string ToString(bool what)
        {
            return "RegexpPositionFunction";
        }
    }
}
