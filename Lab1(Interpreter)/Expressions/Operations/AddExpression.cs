﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER
{
    public class AddExpression : BinaryExpression
    {
        public AddExpression(BaseExpression left = null, BaseExpression right = null) 
            : base(left, right)
        {

        }

        public override object Interpret(Context context)
        {
            object a = base.mLeftOperand.Interpret(context);
            object b = base.mRightOperand.Interpret(context);

            if (a is int && b is int)
            {
                return Convert.ToInt32(a) + Convert.ToInt32(b);
            }
            else if (a is string && b is string)
            {
                return Convert.ToString(a) + Convert.ToString(b);
            }
            else if (a is int && b is string)
            {
                return Convert.ToInt32(a) + Convert.ToString(b);
            }
            else if (a is string && b is int)
            {
                return Convert.ToString(a) + Convert.ToInt32(b);
            }

            return null; 
        }

        public override string ToString(bool what)
        {
            return "+" + "\n" + mLeftOperand.ToString(true) +
                "\t" + mRightOperand.ToString(true);

            return string.Format("+ \n\t1. {0}\n\t2. {1}",
                mLeftOperand.ToString(true) ?? "null",
                mRightOperand.ToString(true) ?? "null");
        }
    }


}
