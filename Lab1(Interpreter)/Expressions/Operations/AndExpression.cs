﻿using INTERPRETER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER.OperatorExpressions
{
    class AndExpression : BinaryExpression
    {
        public AndExpression(BaseExpression left = null, BaseExpression right = null)
            : base(left, right)
        {

        }

        public override object Interpret(Context context)
        {
            object a = base.mLeftOperand.Interpret(context);
           

            object b = base.mRightOperand.Interpret(context);

            if (a is int)
            {
                if (Convert.ToInt32(a) < 0)
                    return false;
                return (Convert.ToInt32(a) > 0 && Convert.ToInt32(b) > 0);
            }
            else if (a is bool)
            {
                if (Convert.ToInt32(a) < 0)
                    return false;
                return (Convert.ToBoolean(a) && Convert.ToBoolean(b));
            }

            return null;
        }

        public override string ToString(bool what)
        {
            return "&&" + "\n" + mLeftOperand.ToString(true) +
               "\t" + mRightOperand.ToString(true);
        }
    }
}
