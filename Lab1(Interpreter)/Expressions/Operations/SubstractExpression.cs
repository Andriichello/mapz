﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER
{
    class SubstractExpression : BinaryExpression
    {
        public SubstractExpression(BaseExpression left = null, BaseExpression right = null)
            : base(left, right)
        {

        }

        public override object Interpret(Context context)
        {
            object a = base.mLeftOperand.Interpret(context);
            object b = base.mRightOperand.Interpret(context);

            if (a is int && b is int) 
            {
                return Convert.ToInt32(a) - Convert.ToInt32(b);
            }
            if (a is string && b is string)
            {
                string result = Convert.ToString(a);
                result = result.Replace(Convert.ToString(b), "");
                return result;
            }
            if (a is string && b is int)
            {
                string result = Convert.ToString(a);
                int amount = Convert.ToInt32(b);
                if (result.Count() - amount > 0)
                    result = result.Substring(0, result.Count() - amount);
                
                result = result.Replace(Convert.ToString(b), "");
                return result;
            }


            return null;
        }

        public override string ToString(bool what)
        {
            dynamic left = mLeftOperand;
            dynamic right = mRightOperand;
            return "-" + "\n" + "\tleft: " + left.ToString(true) + "\n" + "\tright: " + right.ToString(true);

            return string.Format("-: \n\t1. {0}\n\t2. {1}",
                mLeftOperand.ToString(true) ?? "null",
                mRightOperand.ToString(true) ?? "null");
        }

    }
}
