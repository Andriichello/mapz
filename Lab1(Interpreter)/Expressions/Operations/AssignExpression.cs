﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER
{
    class AssignExpression : BinaryExpression
    {
        public AssignExpression(BaseExpression left = null, BaseExpression right = null)
            : base(left, right)
        {

        }

        public override object Interpret(Context context)
        {
            object a = LeftOperand.Interpret(context);
            object b = RightOperand.Interpret(context);

            if (b is string)
            {
                string name = LeftOperand.token.Data;
                object value = new object();
                value = Convert.ToString(b);

                context.AddValue(LeftOperand.token.Data, b);
                return Convert.ToString(b);
            }
            if (b is int)
            {
                string name = LeftOperand.token.Data;
                object value = new object();
                value = Convert.ToInt32(b);
                context.AddValue(LeftOperand.token.Data, b);
                return Convert.ToInt32(b);
            }
            if (b is FunctionDefinitionExpression)
            {
                if (a is FunctionExpression)
                {
                    if (a is RegexpFunction)
                    {
                        return a;
                    }
                    ((FunctionExpression)a).Function = (FunctionDefinitionExpression)b;
                }
                string name = LeftOperand.token.Data;
                context.AddValue(LeftOperand.token.Data, (FunctionDefinitionExpression)b);
                return b;
            }

            return LeftOperand;
        }

        public override string ToString(bool what)
        {
            return "=" + "\n" + mLeftOperand.ToString(true) +
                "\t" + mRightOperand.ToString(true);

            return string.Format("= \n\t1. {0}\n\t2. {1}",
                mLeftOperand.ToString(true) ?? "null",
                mRightOperand.ToString(true) ?? "null");
        }
    }
}
