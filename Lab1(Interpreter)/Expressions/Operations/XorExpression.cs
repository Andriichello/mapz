﻿using INTERPRETER;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTERPRETER.OperatorExpressions
{
    class XorExpression : BinaryExpression
    {
        public XorExpression(BaseExpression left = null, BaseExpression right = null)
            : base(left, right)
        {

        }

        public override object Interpret(Context context)
        {
            object a = base.mLeftOperand.Interpret(context);
            object b = base.mRightOperand.Interpret(context);

            if (a is int)
            {
                if (Convert.ToInt32(a) > 0)
                {
                    if (b is int)
                        return Convert.ToInt32(b) < 0;
                    if (b is string)
                        return !Convert.ToString(b).Contains("true");
                }
                else
                {
                    if (b is int)
                        return Convert.ToInt32(b) > 0;
                    if (b is string)
                        return Convert.ToString(b).Contains("true");
                }
            }
            if (a is string)
            {
                if (Convert.ToString(a).Contains("true"))
                {
                    if (b is int)
                        return Convert.ToInt32(b) < 0;
                    if (b is string)
                        return !Convert.ToString(b).Contains("true");
                }
                else
                {
                    if (b is int)
                        return Convert.ToInt32(b) > 0;
                    if (b is string)
                        return Convert.ToString(b).Contains("true");
                }
            }
            return null;
        }

        public override string ToString(bool what)
        {
            return "&&" + "\n" + mLeftOperand.ToString(true) +
               "\t" + mRightOperand.ToString(true);
        }
    }
}
