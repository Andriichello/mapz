﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTERPRETER
{
    public abstract class BaseExpression
    {
        public BaseExpression(int priority)
        {
            mPriority = 0;
        }

        protected Token mToken;
        public Token token
        {
            get { return this.mToken; }
            set { this.mToken = value; }
        }

        protected int mPriority;
        public int Priority
        {
            get { return mPriority; }
            set { mPriority = value; }
        }

        public virtual object Interpret(Context context) { return null; }
        public void Optimize() { return; }
        public virtual string ToString(bool what) { return "BaseExpression"; }
        public TreeNode ToTreeNode() 
        {
            string str;
            
            return new TreeNode(this.GetType().Name);

            //if (token != null)
            //{
            //    return new TreeNode(token.Type);
            //}
            //else
            //    return new TreeNode(this.GetType().Name);
        }
    }
}
