﻿using MAPZ_Cities_Game_take_1.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Cities_Game_take_1.Areas
{
    public class Town
    {
        private List<Resource> mResources;
        private List<Area> mAreas;
        private uint mPopulation;

        private Town()
        {
            mResources = new List<Resource>();
            mResources.Add(new Resource(RESOURCE_TYPE.WOOD, 0));
            mResources.Add(new Resource(RESOURCE_TYPE.ROCK, 0));

            mAreas = new List<Area>();
            mPopulation = 0;
        }

        public Town(List<Resource> mResources, List<Area> mAreas, uint mPopulation)
            : this()
        {
            if (mResources != null)
            {
                foreach (Resource given in mResources)
                {
                    foreach (Resource current in this.mResources)
                    {
                        if (current != null && current.type == given.type)
                        {
                            current.IncreaseAmount(given.amount);
                            break;
                        }
                    }
                }
            }
            
            if (mAreas != null)
            {
                foreach (Area area in mAreas)
                {
                    this.mAreas.Add(area);
                }
            }
            
            this.mPopulation = mPopulation;
        }

        public List<Resource> GetResources()
        {
            return mResources;
        }

        public uint GetPopulation()
        {
            return mPopulation;
        }

        public uint CountWorkingPopulation()
        {
            uint count = 0;
            foreach (Area area in mAreas)
            {
                if (area != null && area.GetBuilding() != null)
                {
                    count += area.GetBuilding().GetWorkers();
                }
            }

            return count;
        }
        
        public void IncrementPopulation()
        {
            uint newPopulation = mPopulation;
            // calculations here
            double increment = 0;
            if (newPopulation < 20)
            {
                increment = newPopulation * 1.3;
            }
            else
            {
                increment = newPopulation * 1.15;
            }

            if (increment < 1.0)
            {
                increment = 1;
            }
            newPopulation += (uint)increment;

            mPopulation = newPopulation;
        }
        
        private void AddToResources(Resource income)
        {
            if (income == null)
                return;

            foreach (Resource resource in mResources)
            {
                if (resource != null && resource.type == income.type)
                {
                    resource.IncreaseAmount(income.amount);
                    break;
                }
            }
        }

        public void IncrementResources()
        {
            foreach (Area area in mAreas)
            {
                if (area != null && area.GetBuilding() != null)
                {
                    AddToResources(area.GetBuilding().ObtainIncome());
                }
            }
        }

        public void AddToAreas(Area capturedArea)
        {
            mAreas.Add(capturedArea);
        }

    }
}
