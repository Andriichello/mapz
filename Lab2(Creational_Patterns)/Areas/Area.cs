﻿using MAPZ_Cities_Game_take_1.Buildings;
using MAPZ_Cities_Game_take_1.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace MAPZ_Cities_Game_take_1.Areas
{
    public interface IArea
    {
        bool PlaceBuilding(Building building);
        List<Resource> RemoveBuilding();
        Obstacle RemoveObstacle();
        Resource ObtainResource();
        bool Capture();

        Resource GetResource();
        Obstacle GetObstacle();
        Building GetBuilding();
        bool IsItEmpty();
        bool IsItCaptured();

        bool IsThereBuilding();
        bool IsThereObstacle();
        bool IsThereResource();
    }

    public class Area : IArea
    {
        private Resource mResource;
        private Obstacle mObstacle;
        private Building mBuilding;
        private bool IsEmpty;
        private bool IsCaptured;

        public Area(Resource mResource = null)
        {
            this.mResource = mResource;
            this.mObstacle = null;
            this.mBuilding = null;
            this.IsEmpty = true;
            this.IsCaptured = false;
        }

        public Area(Obstacle mObstacle, Resource mResource = null)
        {
            if (mObstacle != null)
                this.IsEmpty = false;

            this.mResource = mResource;
            this.mObstacle = mObstacle;
            this.mBuilding = null;
            this.IsEmpty = true;
            this.IsCaptured = false;
        }

        public Area(Resource mResource, Building mBuilding, Obstacle mObstacle)
        {
            this.IsEmpty = true;
            this.IsCaptured = false;
            if (mObstacle != null && mBuilding != null)
            {
                mObstacle = null;
            }

            if (mObstacle != null || mBuilding != null)
                this.IsEmpty = false;

            this.mResource = mResource;
            this.mObstacle = mObstacle;
            this.mBuilding = mBuilding;
        }

        public Area Clone()
        {
            Area area = (Area)this.MemberwiseClone();

            if (area.IsEmpty == false)
            {
                if (area.mResource != null)
                {
                    area.mResource = new Resource(this.mResource);
                }
                if (area.mObstacle != null)
                {
                    area.mObstacle = new Obstacle(this.mObstacle);
                }
                if (area.mBuilding != null)
                {
                    area.mBuilding = this.mBuilding.Clone();
                }
            }
            return area;
        }

        #region IArea Implementations
        public Building GetBuilding()
        {
            return mBuilding;
        }

        public Obstacle GetObstacle()
        {
            return mObstacle;
        }

        public Resource GetResource()
        {
            return mResource;
        }

        public Resource ObtainResource()
        {
            if (mBuilding != null && mResource != null)
            {
                Resource income = new Resource(mBuilding.ObtainIncome());
                if (income.amount <= mResource.amount)
                {
                    uint newAmount = mResource.amount - income.amount;
                    mResource.ResetAmount();
                    if (newAmount != 0)
                        mResource.IncreaseAmount(newAmount);
                    else
                        mResource = null;
                }
                else
                {
                    income.ResetAmount();
                    income.IncreaseAmount(mResource.amount);
                    mResource = null;
                }

                return income;
            }
            return null;
        }

        public bool PlaceBuilding(Building building)
        {
            if (IsEmpty)
            {
                mBuilding = building;
                IsEmpty = false;
                return true;
            }

            return false;
        }

        public List<Resource> RemoveBuilding()
        {
            if (mBuilding == null)
                return null;

            List<Resource> cashback = mBuilding.GetPrice(mBuilding.GetLevel());
            mBuilding = null;
            IsEmpty = true;
            return cashback;
        }

        public bool IsItEmpty()
        {
            return IsEmpty;
        }

        public Obstacle RemoveObstacle()
        {
            Obstacle obstacle = this.mObstacle;
            return obstacle;
        }

        public bool IsItCaptured()
        {
            return IsCaptured;
        }

        public bool IsThereBuilding()
        {
            if (mBuilding != null)
                return true;

            return false;
        }

        public bool IsThereObstacle()
        {
            if (mObstacle != null)
                return true;

            return false;
        }

        public bool IsThereResource()
        {
            if (mResource != null)
                return true;

            return false;
        }

        public bool Capture()
        {
            if (IsCaptured)
            {
                return false;
            }
            else
            {
                IsCaptured = true;
                return true;
            }
        }
        #endregion

        public string ShortStringRepresentation()
        {
            string representation = "";
            if (IsItCaptured() == true)
            {
                representation += "C";
            }
            else
            {
                representation += "F";
            }

            if (IsThereBuilding() == true)
            {
                representation += "B";
            }
            if (IsThereObstacle() == true)
            {
                representation += "O";
            }
            if (IsThereResource() == true)
            {
                representation += "R";
            }

            return representation;
        }

        
    }
}
