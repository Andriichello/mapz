﻿using MAPZ_Cities_Game_take_1.Areas;
using MAPZ_Cities_Game_take_1.MVC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAPZ_Cities_Game_take_1
{
    public interface IGameView
    {
        void UpdateCell(uint row, uint col, string text);

        void ShowMap(List<List<string>> map);
        void ShowInfo(List<string> info);
        void ShowActions(List<string> actions);
    }


    public partial class PlaygroundForm : Form, IGameView
    {
        private GameController gameController;

        public PlaygroundForm()
        {
            this.gameController = new GameController(this);
            InitializeComponent();
            SetUpGrid();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            uint rows = 8;
            uint cols = 8;

            gameController.CreateNewGame(rows, cols, 2);
        }

        private void btnBackToMenu_Click(object sender, EventArgs e)
        {

        
        }

        #region MAP
        private void SetUpGrid()
        {
            mapGridView.AllowUserToAddRows = false;
            mapGridView.AllowUserToResizeColumns = false;
            mapGridView.AllowUserToResizeRows = false;

            mapGridView.ColumnHeadersVisible = false;
            mapGridView.RowHeadersVisible = false;
            mapGridView.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            mapGridView.GridColor = Color.Black;
        }

        public void ShowMap(List<List<string>> map)
        {
            int sideSize = mapGridView.Width / map[0].Count;

            mapGridView.ColumnCount = (int)map[0].Count;
            mapGridView.Rows.Clear();
            for (int i = 0; i < map.Count; i++)
            {
                string[] rowData = new string[map[i].Count];
                for (int j = 0; j < map[i].Count; j++)
                {
                    rowData[j] = map[i][j];
                }
                mapGridView.Rows.Add(rowData);
                mapGridView.Rows[i].Height = sideSize;
            }

            foreach (DataGridViewColumn column in mapGridView.Columns)
            {
                column.Width = sideSize;
            }
        }


        #endregion

        private void PlaygroundForm_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 5; i++)
            lbActions.Items.Add("item " + i);
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lbActions_DoubleClick(object sender, EventArgs e)
        {
            gameController.ActionSelected((uint)mapGridView.SelectedCells[0].RowIndex, (uint)mapGridView.SelectedCells[0].ColumnIndex, lbActions.SelectedItem.ToString());
        }

        private void mapGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            gameController.CellSelected((uint)mapGridView.SelectedCells[0].RowIndex, (uint)mapGridView.SelectedCells[0].ColumnIndex);
        }

        public void ShowInfo(List<string> info)
        {
            lbInfo.Items.Clear();
            if (info == null)
                return;

            foreach (string sentence in info)
            {
                lbInfo.Items.Add(sentence);
            }
        }

        public void ShowActions(List<string> actions)
        {
            lbActions.Items.Clear();
            if (actions == null)
                return;

            foreach (string sentence in actions)
            {
                lbActions.Items.Add(sentence);
            }
        }

        private void lbInfo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void mapGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void UpdateCell(uint row, uint col, string text)
        {
            mapGridView.Rows[(int)row].Cells[(int)col].Value = text;
        }
    }
}
