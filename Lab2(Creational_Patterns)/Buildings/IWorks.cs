﻿using MAPZ_Cities_Game_take_1.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Cities_Game_take_1.Buildings
{
    public class WoodWorks : Building
    {
        private static readonly uint DEFAULT_CAPACITY = 20;
        private static readonly Resource DEFAULT_INCOME = 
            new Resource(RESOURCE_TYPE.WOOD, 15);
        private static readonly List<Resource> DEFAULT_PRICE =
            new List<Resource>() {
                new Resource(RESOURCE_TYPE.WOOD, 30),
                new Resource(RESOURCE_TYPE.ROCK, 10)
            };

        public WoodWorks()
            : base(DEFAULT_PRICE, DEFAULT_INCOME, DEFAULT_CAPACITY, 1)
        {
            // TEST code
            base.mCurrentIncome = new Resource(RESOURCE_TYPE.WOOD, 30);
        }

        public override uint GetWorkers()
        {
            return mCurrentAmount;
        }

        public override Building Clone()
        {
            WoodWorks works = (WoodWorks)this.MemberwiseClone();

            works.mDefaultIncome = new Resource(this.mDefaultIncome);
            works.mDefaultPrice = new List<Resource>(this.mDefaultPrice);

            works.mCurrentIncome = new Resource(this.mCurrentIncome);
            works.mIncome = new Resource(this.mCurrentIncome);
            works.mPrice = new List<Resource>(this.mPrice);

            return (Building)works;
        }
    }

    public class RockWorks : Building
    {
        private static readonly uint DEFAULT_CAPACITY = 20;
        private static readonly Resource DEFAULT_INCOME =
            new Resource(RESOURCE_TYPE.ROCK, 10);
        private static readonly List<Resource> DEFAULT_PRICE =
            new List<Resource>() {
                new Resource(RESOURCE_TYPE.WOOD, 30),
                new Resource(RESOURCE_TYPE.ROCK, 10)
            };

        public RockWorks()
            : base(DEFAULT_PRICE, DEFAULT_INCOME, DEFAULT_CAPACITY, 1)
        {

        }

        public override uint GetWorkers()
        {
            return mCurrentAmount;
        }

        public override Building Clone()
        {
            RockWorks works = (RockWorks)this.MemberwiseClone();
            
            works.mDefaultIncome = new Resource(this.mDefaultIncome);
            works.mDefaultPrice = new List<Resource>(this.mDefaultPrice);

            works.mCurrentIncome = new Resource(this.mCurrentIncome);
            works.mIncome = new Resource(this.mCurrentIncome);
            works.mPrice = new List<Resource>(this.mPrice);

            return (Building)works;
        }
    }
}
