﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MAPZ_Cities_Game_take_1.Resources;

namespace MAPZ_Cities_Game_take_1.Buildings
{   
    public interface IBuilding
    {
        bool Upgrade(ref List<Resource> userResources);
        Resource ObtainIncome();
        uint IncreaseCurrentAmount(uint numberOfPeople);
        uint DecreaseCurrentAmount(uint numberOfPeople);

        uint GetWorkers();
        uint GetCapacity(uint level);
        List<Resource> GetPrice(uint level);
        Resource GetIncome(uint level);
        uint GetLevel();
    }

    public abstract class Building : IBuilding
    {
        // This values is used in current price, income and capacity calculations 
        #region Default Values
        protected List<Resource> mDefaultPrice;
        protected Resource mDefaultIncome;
        protected uint mDefaultCapacity;
        #endregion

        protected List<Resource> mPrice;
        protected Resource mIncome;
        protected uint mCapacity;

        // current amount is amount of people in the building right now
        // the bigger amount is the more resource you'll get
        protected uint mCurrentAmount;
        protected Resource mCurrentIncome;
        protected uint mLevel;

        protected Building(List<Resource> mDefaultPrice, Resource mDefaultIncome, uint mDefaultCapacity, uint mLevel)
        {
            this.mDefaultPrice = mDefaultPrice;
            this.mDefaultIncome = mDefaultIncome;
            this.mDefaultCapacity = mDefaultCapacity;

            this.mCurrentAmount = 0;
            this.mCurrentIncome = null;
            SetLevel(mLevel);
        }

        public abstract Building Clone();

        protected void SetLevel(uint level)
        {
            this.mLevel = level;
            this.mCapacity = CalculateLevelCapacity(level);
            this.mIncome = CalculateLevelIncome(level);
            this.mPrice = CalculateLevelPrice(level);
        }

        protected uint CalculateLevelCapacity(uint level)
        {
            if (level == 0)
                return 0;


            uint capacity = mDefaultCapacity;
            // calculations here
            capacity *= level;

            return capacity;
        }

        protected Resource CalculateLevelIncome(uint level)
        {
            if (level == 0 || mDefaultIncome == null)
                return null;
            
            Resource income;
            // calculations here
            income = new Resource(mDefaultIncome.type,
                mDefaultIncome.amount * level);

            return income;
        }

        protected List<Resource> CalculateLevelPrice(uint level)
        {
            if (level == 0 || mDefaultPrice == null)
                return null;

            List<Resource> price;
            price = new List<Resource>();
            foreach (Resource res in mDefaultPrice)
            {
                if (mDefaultIncome != null)
                {
                    // calculations here
                    price.Add(new Resource(mDefaultIncome.type,
                    mDefaultIncome.amount * level));
                }
            }
            return price;
        }


        #region IBuilding Implementations
        public bool Upgrade(ref List<Resource> userResources)
        {
            if (userResources == null || userResources.Count == 0)
            {
                return false;
            }

            for (int i = 0; i < userResources.Count; i++)
            {
                foreach (Resource price in mPrice)
                {
                    if (userResources.ElementAt(i).type == price.type)
                    {
                        if (userResources.ElementAt(i).amount < price.amount)
                        {
                            return false;
                        }
                    }
                }  
            }

            for (int i = 0; i < userResources.Count; i++)
            {
                foreach (Resource price in mPrice)
                {
                    if (userResources.ElementAt(i).type == price.type)
                    {
                        Resource reduced = new Resource(userResources[i].type,
                            userResources[i].amount - price.amount);
                    }
                }
            }

            SetLevel(mLevel + 1);
            return true;
        }

        public Resource ObtainIncome()
        {
            if (mCurrentIncome == null)
                return null;

            Resource income = new Resource(mCurrentIncome);
            mCurrentIncome.ResetAmount();
            return income;
        }

        public uint IncreaseCurrentAmount(uint numberOfPeople)
        {
            if (mCapacity < (mCurrentAmount + numberOfPeople))
            {
                numberOfPeople -= mCapacity - mCurrentAmount;
                mCurrentAmount = mCapacity;
                return numberOfPeople;
            }

            return 0;
        }

        public uint DecreaseCurrentAmount(uint numberOfPeople)
        {
            if (mCurrentAmount < numberOfPeople)
            {
                numberOfPeople -= mCurrentAmount;
                mCurrentAmount = 0;
            }
            else
            {
                mCurrentAmount -= numberOfPeople;
            }
            
            return numberOfPeople;
        }

        public uint GetCapacity(uint level)
        {
            return CalculateLevelCapacity(level);
        }

        public List<Resource> GetPrice(uint level)
        {
            return CalculateLevelPrice(level);
        }

        public Resource GetIncome(uint level)
        {
            return CalculateLevelIncome(level);
        }

        public uint GetLevel()
        {
            return mLevel;
        }

        public virtual uint GetWorkers()
        {
            return 0;
        }
        #endregion

    }

    #region Factory Interfaces
    public interface IFactory
    {
        Building CreateBuilding();
    }
    #endregion

    #region Factories
    public class WoodWorksFactory : IFactory
    {
        public Building CreateBuilding()
        {
            return new WoodWorks();
        }
    }

    public class RockWorksFactory : IFactory
    {
        public Building CreateBuilding()
        {
            return new RockWorks();
        }
    }

    public class HouseFactory : IFactory
    {
        public Building CreateBuilding()
        {
            return new House();
        }
    }
    #endregion

}
