﻿using MAPZ_Cities_Game_take_1.Areas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Cities_Game_take_1.MVC
{
    public interface IGamePlay
    {
        void CreateNewGame(uint rows, uint cols, uint towns);

        void CellSelected(uint row, uint col);
        void ActionSelected(uint row, uint col, string action);
        //void CaptureArea(uint row, uint col);
        //void RemoveObstacle(uint row, uint col);
        //void PlaceBuilding(uint row, uint col);
    }

    public class GameController : IGamePlay
    {
        private IMapManagementFacade gameManager;
        private IGameView gameView;

        public GameController(IGameView gameView)
        {
            this.gameView = gameView;
            this.gameManager = new MapManagementFacade();
        }

        public void ActionSelected(uint row, uint col, string action)
        {
            Area area = GameModel.Instance.GetArea(row, col);
            if (area == null)
                return;

            if (action == "Capture Area")
            {
                gameManager.CapureArea(row, col);
            }
            else if (action == "Remove Building")
            {
                gameManager.RemoveBuilding(row, col);
            }
            else if (action == "Place Building")
            {
                gameManager.PlaceHouse(row, col);
            }
            else if (action == "Place House")
            {
                gameManager.PlaceHouse(row, col);
            }
            else if (action == "Place Wood Works")
            {
                gameManager.PlaceHouse(row, col);

            }
            else if (action == "Place Rock Works")
            {
                gameManager.PlaceHouse(row, col);
            }
            gameView.UpdateCell(row, col, area.ShortStringRepresentation());
            CellSelected(row, col);
        }

        public void CaptureArea(uint row, uint col)
        {
            throw new NotImplementedException();
        }

        public void CellSelected(uint row, uint col)
        {
            Area area = GameModel.Instance.GetArea(row, col);
            List<string> info = new List<string>();
            List<string> actions = new List<string>();

            if (area == null || area.IsItCaptured())
            {
                info.Add("no information available");
                actions.Add("no actions available");
            }
            else
            {
                string data = "";
                if (area.IsItCaptured() == false)
                {
                    actions.Add("Capture Area");
                }
                if (area.IsItEmpty())
                {
                    actions.Add("Place House");
                }
                if (area.IsThereResource())
                {
                    data = "Resource: ";

                    if (area.GetResource().type == Resources.RESOURCE_TYPE.WOOD)
                    {
                        data += "Wood";
                        if (area.IsThereBuilding() == false)
                            actions.Add("Place Wood Works");
                    }
                    if (area.GetResource().type == Resources.RESOURCE_TYPE.ROCK)
                    {
                        data += "Rock";
                        if (area.IsThereBuilding() == false)
                            actions.Add("Place Rock Works");
                    }
                    info.Add(data + ", " + area.GetResource().amount);
                }
                if (area.IsThereBuilding())
                {
                    data = "Building: ";
                    info.Add(data + area.GetBuilding().GetType().Name);
                    actions.Add("Remove Building");
                }
                if (area.IsThereObstacle())
                {
                    data = "Obstacle: ";
                    if (area.GetObstacle().GetResource().type == Resources.RESOURCE_TYPE.WOOD)
                    {
                        data += "Wood";
                    }
                    if (area.GetObstacle().GetResource().type == Resources.RESOURCE_TYPE.ROCK)
                    {
                        data += "Rock";
                    }

                    info.Add(data + ", " + area.GetObstacle().GetResource().amount);
                    actions.Add("Remove Obstacle");
                }
            }

            gameView.ShowInfo(info);
            gameView.ShowActions(actions);

        }

        public void CreateNewGame(uint rows, uint cols, uint players)
        {
            gameManager.GenerateMap(rows, cols, players);
            GameModel model = GameModel.Instance;

            List<List<string>> playground = new List<List<string>>();
            for (int i = 0; i < rows; i++)
            {
                playground.Add(new List<string>());
                for (int j = 0; j < cols; j++)
                {
                    playground[i].Add(model.map[i * (int)cols + j].ShortStringRepresentation());
                    //playground[i].Add(Convert.ToString(i * cols + j));
                }
            }
            gameView.ShowMap(playground);
        }

        public void PlaceBuilding(uint row, uint col)
        {
            // do something here
            return;
        }

        public void RemoveObstacle(uint row, uint col)
        {
            // do something here
            return;
        }
    }
}
