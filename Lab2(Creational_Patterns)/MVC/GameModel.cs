﻿using MAPZ_Cities_Game_take_1.Areas;
using MAPZ_Cities_Game_take_1.Buildings;
using MAPZ_Cities_Game_take_1.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Cities_Game_take_1.MVC
{
    // this class will be a Singleton
    sealed class GameModel
    {
        private static GameModel instance = new GameModel();
        private uint numberOfInstances = 0;
        
        private GameModel()
        {
            map = null;
            towns = null;
            player = 0;
            rows = columns = 0;
            numberOfInstances++;
        }

        public static GameModel Instance
        {
            get
            {
                return instance;
            }
        }

        public Area GetArea(uint row, uint col)
        {
            int subscript = (int)(row * columns + col);
            if (subscript >= map.Count)
                return null;

            return map.ElementAt(subscript);
        }

        public uint NextTown()
        {
            player++;
            if (player == towns.Count)
            {
                player--;
            }

            return player;
        }

        public uint player { get; private set; }
        public uint rows { get; set; }
        public uint columns { get; set; }
        public List<Area> map { get; set; }
        public List<Town> towns { get; set; }
    }
}
