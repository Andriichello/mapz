﻿using MAPZ_Cities_Game_take_1.Areas;
using MAPZ_Cities_Game_take_1.Buildings;
using MAPZ_Cities_Game_take_1.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Cities_Game_take_1.MVC
{
    public interface IMapManagementFacade
    {
        void GenerateMap(uint rows, uint columns, uint towns);
        void RemoveBuilding(uint row, uint column);
        void PlaceWoodWorks(uint row, uint column);
        void PlaceRockWorks(uint row, uint column);
        void PlaceHouse(uint row, uint column);
        void CapureArea(uint row, uint column);
    }

    public class MapManagementFacade : IMapManagementFacade
    {
        private static Random random = new Random();
        private static HouseFactory houseFactory = new HouseFactory();
        private static WoodWorksFactory woodWorksFactory = new WoodWorksFactory();
        private static RockWorksFactory rockWorksFactory = new RockWorksFactory();

        public void GenerateMap(uint rows, uint columns, uint towns)
        {
            GameModel model = GameModel.Instance;

            model.rows = rows;
            model.columns = columns;

            model.map = new List<Area>();

            for (int i = 0; i < rows * columns; i++)
            {
                // areas creation
                Resource resource = null;
                Obstacle obstacle = null;
                Building building = null;

                // optional creation of resource
                if (GenerateRandomBool() == true)
                {
                    resource = GenerateRandomResource(1000, 10000);

                    // optional creation of works building
                    if (GenerateRandomBool() == true)
                    {
                        if (resource.type == RESOURCE_TYPE.WOOD)
                        {
                            building = woodWorksFactory.CreateBuilding();
                        }
                        if (resource.type == RESOURCE_TYPE.ROCK)
                        {
                            building = rockWorksFactory.CreateBuilding();
                        }
                    }
                }
                // optional creation of house
                else if (GenerateRandomBool() == true)
                {
                    building = houseFactory.CreateBuilding();
                }
                // optional creation of obstacle
                else if (GenerateRandomBool() == true)
                {
                    obstacle = GenerateRandomObstacle(20, 100);
                }

                Area area = new Area(resource, building, obstacle);
                model.map.Add(area);
            }


            model.towns = new List<Town>();
            // creating and populating towns
            for (int i = 0; i < towns; i++)
            {
                List<Resource> townResources = new List<Resource>();
                townResources.Add(new Resource(RESOURCE_TYPE.WOOD, 300));
                townResources.Add(new Resource(RESOURCE_TYPE.ROCK, 150));

                Town town = new Town(townResources, null, 100);
                model.towns.Add(town);
            }
        }

        private bool GenerateRandomBool(uint range = 100)
        {
            return ((random.Next(0, (int)range) % 2) == 0);
        }

        private Resource GenerateRandomResource(uint minAmount, uint maxAmount)
        {
            uint amount = (uint)random.Next((int)minAmount, (int)maxAmount);
            RESOURCE_TYPE type = RESOURCE_TYPE.WOOD;
            if (GenerateRandomBool() == true)
                type = RESOURCE_TYPE.WOOD;
            else
                type = RESOURCE_TYPE.ROCK;

            return new Resource(type, amount);
        }

        private Obstacle GenerateRandomObstacle(uint minAmount, uint maxAmount)
        {
            uint amount = (uint)random.Next((int)minAmount, (int)maxAmount);
            RESOURCE_TYPE type = RESOURCE_TYPE.WOOD;
            if (GenerateRandomBool() == true)
                type = RESOURCE_TYPE.WOOD;
            else
                type = RESOURCE_TYPE.ROCK;

            return new Obstacle(new Resource(type, amount));
        }

        public void PlaceWoodWorks(uint row, uint column)
        {
            Area area = GameModel.Instance.GetArea(row, column);
            if (area == null)
                return;
            area.PlaceBuilding(woodWorksFactory.CreateBuilding());
        }

        public void PlaceRockWorks(uint row, uint column)
        {
            Area area = GameModel.Instance.GetArea(row, column);
            if (area == null)
                return;
            area.PlaceBuilding(rockWorksFactory.CreateBuilding());
        }

        public void PlaceHouse(uint row, uint column)
        {
            Area area = GameModel.Instance.GetArea(row, column);
            if (area == null)
                return;
            area.PlaceBuilding(houseFactory.CreateBuilding());
        }

        public void CapureArea(uint row, uint col)
        {
            GameModel model = GameModel.Instance;
            Area area = model.GetArea(row, col);
            if (area == null)
                return;
            if (area.Capture())
            {
                model.towns[(int)model.player].AddToAreas(area);
            }
        }

        public void RemoveBuilding(uint row, uint col)
        {
            GameModel model = GameModel.Instance;
            Area area = model.GetArea(row, col);
            if (area == null)
                return;
            area.RemoveBuilding();
        }
    }
}
